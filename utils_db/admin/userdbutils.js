var dbutils = require('../db.js')

module.exports = {
    /**
     * 根据渠道查询用户
     * @param channel
     * @param callback
     */
    getUserByChannel: function (channel, callback) {
        dbutils.query("SELECT * FROM user WHERE channel=?", channel, function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 用户openid查询用户的充值详情
     * @param openid
     * @param callback
     */
    getRechargeByUser: function (openid, callback) {
        dbutils.query("SELECT * FROM record WHERE record WHERE openid=? and type=4", openid, function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 根据渠道名查到充值记录
     */
    getRecordByChannel: function (channel,callback) {
        /**
          SELECT * FROM record WHERE openid IN (SELECT openid FROM user WHERE channel="self") and type=4;
         */
        dbutils.query("SELECT * FROM record WHERE openid IN (SELECT openid FROM user WHERE channel=?) and type=1;", channel, function (results, fields) {
            callback(results,fields)
        })
    }
}