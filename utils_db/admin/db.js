var mysql = require('mysql');

var db_config = {
    host: '127.0.0.1',
    user: 'root',
    password: '1234qwer',
    database: 'orange'
}

module.exports = {
    query: function (sql, params) {

        return new Promise(function (resolve, reject) {
            var connection = mysql.createConnection(db_config);//创建连接
            //开启连接,并做报错处理
            connection.connect(function (err) {
                if (err) {
                    console.log("ERROR_DB:开启数据库连接失败!")
                    reject(err)
                    throw err;
                }
            })
            //执行数据库各种操作
            connection.query(sql, params, function (error, results, fields) {
                if (error) {
                    console.log("ERROR_DB:数据库操作失败!")
                    reject(error)
                    throw error;
                }
                resolve(results)
            })
            //完成后,关闭数据库连接, 流程结束
            connection.end(function (err) {
                if (err) {
                    console.log("ERROR_DB:数据库关闭失败")
                    reject(err)
                    throw err;
                }
            })
        })
    }
}