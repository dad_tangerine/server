var db = require('./db.js')

module.exports = {
    findUsers: async function (page, size, startTime, endTime) {
        var results = await db.query("SELECT * FROM user WHERE time>? and time<? limit ?,?", [startTime, endTime, page*size, size])
        return results
    },
    totalUsers: async function(){
        var results = await db.query("SELECT count(*) AS ? FROM user", "total")
        return results
    }
}