var db = require('./db.js')

module.exports = {
    getRecordByOpenid: async function (openid) {
        var results = await db.query("SELECT * FROM record WHERE openid=?", openid)
        return results
    }
}