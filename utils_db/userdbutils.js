var dbutils = require('./db.js')

module.exports = {
    /**
     * 根据openid获取用户信息
     * @param openid
     */
    getUserInfoByOpenid: function(openid, callback){
        dbutils.query("SELECT * FROM user WHERE openid=?", [openid], function (results, fields) {
            callback(results)
        })
    },
    /**
     * 注册
     * @param openid
     * @param nickName
     * @param avatar
     * @param channel
     * @param gender
     * @param callback
     */
    register: function (openid, nickName, avatar, channel, gender, callback) {
        dbutils.query("INSERT INTO user(openid, channel, nickName, avatarUrl, gender) values(?,?,?,?,?)", [openid, channel, nickName, avatar, gender], function (results, fields) {
            callback(results)
        })
    },
    /**
     * 设置金钱余额
     * @param openid
     * @param new_balance
     * @param callback
     */
    setBalance: function(openid, new_balance, callback){
        dbutils.query("UPDATE user SET balance=? where openid=?", [new_balance, openid], function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 设置橘子余额
     * @param openid
     * @param new_orange
     * @param callback
     */
    setOrange: function (openid, new_orange, callback ) {
        dbutils.query("UPDATE user SET orange=? where openid=?", [new_orange, openid], function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 获取消费记录
     * @param openid
     * @param callback
     */
    getWaterBills: function (openid, callback) {
        dbutils.query("SELECT * FROM trade WHERE openid=?" [openid], function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 修改用户地址
     * @param openid
     * @param addr
     * @param callback
     */
    updateAddr: function(openid, addr, callback){
        dbutils.query("UPDATE user SET addr=? WHERE openid=?", [addr, openid], function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 查询代理商共有多少用户
     * @param callback
     */
    getChannelTotal: function(callback){
        dbutils.query("select channel, count(channel) as total from user group by channel having count(?)>0", "channel", function (results, fields) {
            callback(results, fields)
        })

    }
}