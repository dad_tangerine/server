var dbutils = require('./db.js')

module.exports = {
    addWithDraw: function(type, openid, orange_number, money, orange, addr, tel, consignee, callback){
        dbutils.query("INSERT INTO withdraw(type, openid, orange_number, money, orange, addr, tel, consignee) VALUES(?,?,?,?,?,?,?,?)", [type, openid, orange_number, money, orange, addr, tel, consignee], function (results, fields) {
            callback(results, fields)
        })
    },
    findWithdrawById: function (id, callback) {
        dbutils.query("SELECT * FROM withdraw WHERE id=?", id, function (results, fields) {
            callback(results, fields)
        })
    },
    findWithdrawByOpenid: function (openid, callback) {
        dbutils.query("SELECT * FROM withdraw WHERE openid=? and (state=0 or state=2 or state=3)", openid, function (results, fields) {
            callback(results, fields)
        })
    }
}