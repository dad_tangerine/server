var mysql = require('mysql');

var myConfig = require('../config')


module.exports = {

    /**
     *  橘子清算
     * @param CURRENT_USER 清算的用户
     * @param CURRENT_ROUND 清算的游戏
     * @param return_orange 需要返还的橘子个数
     * @param callback_error
     * @param callback_success
     */
    return_orange: function (CURRENT_USER, CURRENT_ROUND, return_orange, callback_error, callback_success) {
        var new_orange = parseInt(return_orange) + parseInt(CURRENT_USER.orange)
        var connection = mysql.createConnection(myConfig.db_config);//创建连接
        connection.beginTransaction(function (error) {
            if (error) {
                callback_error(error)
                return connection.rollback(function () {
                    console.log("事务[24小时清算橘子]: 开启事务失败")
                    throw error
                })
            }
            //round设置为已经清算, 3
            connection.query("UPDATE round SET state=3 WHERE id=?", [CURRENT_ROUND.id], function (error, results, fields) {
                if (error) {
                    return connection.rollback(function () {
                        callback_error(error)
                        console.log("事务[24小时清算橘子]失败: 修改round状态为3!")
                        throw error
                    })
                }
                //用户余额添加
                connection.query("UPDATE user SET orange=? WHERE openid=?", [new_orange, CURRENT_USER.openid], function (error, results, fields) {
                    if (error) {
                        return connection.rollback(function () {
                            callback_error(error)
                            console.log("事务[24小时清算橘子]失败: 修改用户余额失败!")
                            throw error
                        })
                    }
                    //添加record记录
                    connection.query("INSERT INTO record(openid, rid, type, differ_orange, des, balance_orange, balance_money) VALUES(?, ?, ?, ?, ?, ?, ?)", [CURRENT_USER.openid, CURRENT_ROUND.id, 6, return_orange, "超时返还", new_orange, CURRENT_USER.balance], function (error, results, fields) {
                        if (error) {
                            return connection.rollback(function () {
                                callback_error(error)
                                console.log("事务[24小时清算橘子]失败: 添加记录失败!")
                                throw error
                            })
                        }
                        connection.commit(function (error) {
                            if (error) {
                                return connection.rollback(function () {
                                    callback_error(error)
                                    console.log("事务[24小时清算橘子]失败: 提交失败!")
                                    throw error
                                })
                            }
                            console.log("事务[24小时清算橘子]成功: 返还个数:", return_orange);
                            callback_success()
                            connection.end()
                        })
                    })
                })
            })
        })



    }

}