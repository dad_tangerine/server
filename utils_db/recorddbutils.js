var dbutils = require('./db.js')

module.exports = {

    /**
     * 获取消费记录(所有)
     * @param openid
     * @param callback
     */
    getRecords: function (openid, page, size, callback) {
        dbutils.query("SELECT * FROM record WHERE openid=? limit ?,?", [openid, page, size], function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 根据类型获取消费记录
     * @param openid
     * @param type
     * @param callback
     */
    getRecordsByType: function(openid, type, page, size, callback){
        dbutils.query("SELECT * FROM record WHERE openid=? and type=? limit ?,?", [openid, type, page, size], function (results, fields) {
            callback(results, fields)
        })
    },
    addRecord: function (openid, rid, type, des, balance_orange, balance_money, callback) {
        dbutils.query("INSERT INTO record (openid, rid, type, des, balance_orange, balance_money) values(?,?,?,?,?,?)", [openid, rid, type, des, balance_orange, balance_money], function (results, fields) {
            callback(results,fields)
        })
    },
    /**
     * 获取我的收入记录
     * @param openid
     * @param callback
     */
    getRecordIncome: function(openid, page, size, callback){
        dbutils.query("SELECT * FROM record WHERE openid=? and type=5 order by id desc limit ?,?", [openid, page*size, size], function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 获取我的支出记录
     * @param openid
     * @param callback
     */
    getRecordOutcome: function(openid, page, size, callback){
        dbutils.query("SELECT * FROM record WHERE from_openid=? and type=5 order by id desc limit ?,?", [openid, page*size, size], function(results, fields){
            callback(results, fields)
        })
    },
    /**
     * 获取我的收入总数
     */
    getIncomeTotal: function(openid, callback){
        dbutils.query("SELECT COUNT(*) AS total FROM record WHERE openid=? and type=5", openid, function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 获取我的支出总数
     */
    getOutcomeTotal: function (openid, callback) {
        dbutils.query("SELECT COUNT(*) AS total FROM record WHERE from_openid=? and type=5", openid, function (results, fields) {
            callback(results, fields)
        })
    }

}