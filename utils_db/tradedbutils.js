var dbutils = require('./db.js')

module.exports = {
    /**
     * 创建订单
     * @param openid
     * @param out_trade_no
     * @param total_fee
     * @param detail
     * @param callback_success
     * @param callback_err
     */
    createTrade: function (openid, out_trade_no, total_fee, orange, detail, prepay_id, nonce_str, callback_success) {
        dbutils.query("INSERT INTO trade(openid, out_trade_no, total_fee, orange, detail, prepay_id, nonce_str) values(?, ?, ?, ?, ?, ?, ?)", [openid, out_trade_no, total_fee, orange, detail, prepay_id, nonce_str], function (results, fields) {
            callback_success(results, fields)
        })
    },
    findTrade: function (openid, out_trade_no, total_fee, nonce_str, callback_success) {
        dbutils.query("SELECT * FROM trade WHERE openid=? and out_trade_no=? and total_fee=? and nonce_str=?", [openid, out_trade_no, total_fee, nonce_str], function (results, fields) {
            callback_success(results, fields)
        })
    }
}