var dbutils = require('./db.js')

module.exports = {
    /**
     * 获取指定的参与情况
     * @param id
     * @param callback
     */
    getPartakeByid: function (id, callback) {
        dbutils.query("SELECT * FROM partake WHERE id=?", [id], function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 获取某局游戏的所有参与情况
     * @param rid
     * @param callback
     */
    getPartakesByRid: function (rid, callback) {
        dbutils.query("SELECT * FROM partake WHERE rid=?", [rid], function (results, fields) {
            callback(results, fields)
        })
    },
    /**
     * 获取某局游戏的参与总数
     * @param rid
     * @param callback
     */
    getPartakesCountByRid: function (rid, callback) {
        dbutils.query("SELECT COUNT(*) AS count FROM partake WHERE rid=?", [rid], function (results, fields) {
            callback(results, fields)
        })
    },
    addPartake: function (rid, uid, callback) {
        dbutils.query("INSERT INTO partake(uid, rid) VALUES(?,?)", [uid, rid], function (results, fields) {
            callback()
        })
    }

}