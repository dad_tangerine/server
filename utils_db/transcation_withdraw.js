var mysql = require('mysql');
var myConfig = require('../config')

module.exports = {

    /**
     * 提现事务
     * @param openid
     * @param withdraw_orage 提现橘子数量
     * @param new_orange 账户新余额
     * @param addr 收货地址
     * @param tel 电话
     * @param consignee 收货人
     * @param new_money 账户新余额
     * @param callback_success
     * @param callback_error
     */
    addWithDraw: function (openid, withdraw_orage, new_orange, addr, tel, consignee, new_money, callback_success, callback_error) {
        var connection = mysql.createConnection(myConfig.db_config);//创建连接
        connection.beginTransaction(function (error) {
            if(error){
                callback_error(error)
            }
            console.log("事务[提现}开启成功")
            connection.query("INSERT INTO withdraw(openid, orange_number, orange, addr, tel, consignee) VALUES(?, ?, ?, ?, ?, ?)", [openid, withdraw_orage, withdraw_orage, addr, tel, consignee], function (error, results, fields) {
                if(error){
                    console.log("事务[提现}: 添加提现订单出错")
                    return connection.rollback(function () {
                        callback_error(error)
                        throw error;
                    });
                }
                connection.query("UPDATE user SET orange=? WHERE openid=?", [new_orange, openid], function (error, results, fields) {
                    if(error){
                        console.log("事务[提现}: 减去用户余额出错!")
                        return connection.rollback(function () {
                            callback_error(error)
                            throw error
                        })
                    }
                    connection.query("INSERT INTO record(openid, type, differ_orange, des, balance_orange, balance_money) VALUES(?, ?, ?, ?, ?, ?)", [openid, 6, withdraw_orage, "橘子提现", new_orange, new_money], function (error, results, fields) {
                        if(error){
                            console.log("事务[提现}: 添加消费记录失败!")
                            return connection.rollback(function () {
                                callback_error(error)
                                throw error
                            })
                        }
                        connection.commit(function (error) {
                            if(error){
                                console.log("事务[提现}: 提交失败!")
                                return connection.rollback(function(){
                                    throw error
                                })
                            }
                            callback_success()
                            console.log('事务[提现]成功 - success!');
                            connection.end()
                        })
                    })

                })
            })
        })

    }

}