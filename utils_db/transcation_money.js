var mysql = require('mysql');
var myConfig = require('../config')

module.exports = {

    moneySuccess: function (CURRENT_USER, CURRENT_TRADE, callback_success, callback_error) {

        var connection = mysql.createConnection(myConfig.db_config)
        console.log("事务[支付回调]开启成功>>>>>>>>>>")
        connection.beginTransaction(function (error) {
            if (error) {
                console.log("事务[支付回调]: 开启失败")
                callback_error(error)
            }
            //给用户增加橘子余额, 钱的余额不用改
            connection.query("UPDATE user SET orange=? WHERE openid=?", [CURRENT_USER.orange + CURRENT_TRADE.orange, CURRENT_TRADE.openid], function (error, results, fields) {
                if (error) {
                    return connection.rollback(function () {
                        console.log("事务[支付回调]: 更改用户橘子余额失败, 回滚吧!")
                        callback_error(error)
                        throw error
                    })
                }
                // trade修改订单状态
                connection.query("UPDATE trade SET state=1 WHERE id=?", CURRENT_TRADE.id, function (error, results, fields) {
                    if (error) {
                        return connection.rollback(function () {
                            console.log("事务[支付回调]: 修改订单状态失败,回滚吧!id=", CURRENT_TRADE.id)
                            callback_error(error)
                            throw error
                        })
                    }
                    // record添加充值记录 type=1,
                    connection.query("INSERT INTO record(openid, type, differ_money, des, balance_orange, balance_money) VALUES(?, ?, ?, ?, ?, ?)", [CURRENT_USER.openid, 1, CURRENT_TRADE.total_fee, "充值" + CURRENT_TRADE.total_fee + "分(用具橘子购买)", CURRENT_USER.orange, CURRENT_TRADE.total_fee], function (error, results, fields) {
                        if (error) {
                            return connection.rollback(function () {
                                console.log("事务[支付回调]: 添加充值记录失败, 回滚吧!")
                                callback_error(error)
                                throw error
                            })
                        }
                        // record添加购买记录4,
                        connection.query("INSERT INTO record(openid, type, differ_orange, des, balance_orange, balance_money) VALUES(?, ?, ?, ?, ?, ?)", [CURRENT_USER.openid, 4, CURRENT_TRADE.orange, "购买" + CURRENT_TRADE.orange + "个橘子", CURRENT_USER.orange + CURRENT_TRADE.orange, 0], function (error, results, fields) {
                            if (error) {
                                return connection.rollback(function () {
                                    console.log("事务[支付回调]: 添加购买记录失败, 回滚吧!")
                                    callback_error(error)
                                    throw error
                                })
                            }
                            connection.commit(function (error) {
                                if (error) {
                                    return connection.rollback(function () {
                                        console.log("事务[支付回调]: 提交失败, 回滚吧!")
                                        callback_error(error)
                                        throw error
                                    })
                                }
                                console.log("事务[支付回调]: SUCCESS!")
                                callback_success()
                                connection.end()
                            })
                        })

                    })
                })
            })
        })
    },
    moneySuccess_withdraw: function (CURRENT_USER, CURRENT_TRADE, CURRENT_WITHDRAW, callback_success, callback_error) {
        var connection = mysql.createConnection(myConfig.db_config)
        console.log("事务[提现支付回调]开启成功>>>>>>>>>>")
        connection.beginTransaction(function (error) {
            if (error) {
                console.log("事务[提现支付回调]: 开启失败")
                callback_error(error)
            }
            //user用户橘子余额扣掉
            connection.query("UPDATE user SET orange=? WHERE openid=?", [CURRENT_USER.orange - CURRENT_WITHDRAW.orange, CURRENT_TRADE.openid], function (error, results, fields) {
                if (error) {
                    return connection.rollback(function () {
                        console.log("事务[提现支付回调]: 更改用户橘子余额失败, 回滚吧!")
                        callback_error(error)
                        throw error
                    })
                }
                // trade修改订单状态
                connection.query("UPDATE trade SET state=1 WHERE id=?", CURRENT_TRADE.id, function (error, results, fields) {
                    if (error) {
                        return connection.rollback(function () {
                            console.log("事务[提现支付回调]: 修改订单状态失败,回滚吧!id=", CURRENT_TRADE.id)
                            callback_error(error)
                            throw error
                        })
                    }
                    // record添加充值记录 type=1,
                    connection.query("INSERT INTO record(openid, type, differ_money, des, balance_orange, balance_money) VALUES(?, ?, ?, ?, ?, ?)", [CURRENT_USER.openid, 1, CURRENT_TRADE.total_fee, "充值" + CURRENT_TRADE.total_fee + "分(用来提现的)", CURRENT_USER.orange, CURRENT_TRADE.total_fee], function (error, results, fields) {
                        if (error) {
                            return connection.rollback(function () {
                                console.log("事务[提现支付回调]: 添加(充值)记录失败, 回滚吧!")
                                callback_error(error)
                                throw error
                            })
                        }
                        // record添加记录3-提现用的
                        connection.query("INSERT INTO record(openid, type, differ_orange, des, balance_orange, balance_money) VALUES(?, ?, ?, ?, ?, ?)", [CURRENT_USER.openid, 3, CURRENT_TRADE.orange, "提现" + CURRENT_TRADE.orange + "个橘子(补钱)", CURRENT_USER.orange + CURRENT_TRADE.orange, 0], function (error, results, fields) {
                            if (error) {
                                return connection.rollback(function () {
                                    console.log("事务[提现支付回调]: 添加(提现)记录失败, 回滚吧!")
                                    callback_error(error)
                                    throw error
                                })
                            }
                            //withdraw 改为邮费已经付
                            connection.query("UPDATE withdraw SET payment=1 WHERE id=?", CURRENT_WITHDRAW.id, function(error, results, fields){
                                if (error) {
                                    return connection.rollback(function () {
                                        console.log("事务[提现支付回调]: 修改提现状态为已支付失败了, 回滚吧!")
                                        callback_error(error)
                                        throw error
                                    })
                                }
                                connection.commit(function (error) {
                                    if (error) {
                                        return connection.rollback(function () {
                                            console.log("事务[提现支付回调]: 提交失败, 回滚吧!")
                                            callback_error(error)
                                            throw error
                                        })
                                    }
                                    console.log("事务[提现支付回调]: SUCCESS!")
                                    callback_success()
                                    connection.end()
                                })
                            })


                        })

                    })
                })
            })
        })
    }

}