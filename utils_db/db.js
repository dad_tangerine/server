var mysql = require('mysql');

var db_config = {
    host: '127.0.0.1',
    user: 'root',
    // password: '48a18fb94a694',
    password: '1234qwer',
    database: 'orange'
}

module.exports = {
    query: function (sql, params, callback) {
        var connection = mysql.createConnection(db_config);//创建连接
        //开启连接,并做报错处理
        connection.connect(function (err) {
            if (err) {
                console.log("ERROR_DB:开启数据库连接失败!")
                throw err;
            }
        })
        //执行数据库各种操作
        connection.query(sql, params, function (error, results, fields) {
            if (error) {
                console.log("ERROR_DB:数据库操作失败!")
                throw error;
            }
            //操作成功,执行回调
            callback && callback(results, fields)
        })
        //完成后,关闭数据库连接, 流程结束
        connection.end(function (err) {
            if (err) {
                console.log("ERROR_DB:数据库关闭失败")
                throw err;
            }
        })
    },
    /**
     * 领取逻辑事务处理
     * @param openid 用户openid
     * @param rid 游戏id
     * @param new_orange 用户的新橘余额
     * @param recordObj 记录json数据
     */
    doTranscation: function (openid, rid, call, from_openid, new_orange, recordObj, callback_error, callback_commit) {
        var connection = mysql.createConnection(db_config);//创建连接
        //开始处理事务
        connection.beginTransaction(function (err) {
            if (err) {
                throw err;
                console.log("开启事务[领橘子]失败")
            }

            // partake 添加数据
            connection.query('INSERT INTO partake(uid, rid, subject) VALUES(?,?,?)', [openid, rid, call], function (error, results, fields) {
                if (error) {
                    callback_error(error)
                    console.log("事务[领橘子]第一步(添加partake)失败, 回滚吧")
                    return connection.rollback(function () {
                        callback_error(error)
                        throw error;
                    });
                }
                // user 余额增加
                connection.query('UPDATE user SET orange=? where openid=?', [new_orange, openid], function (error, results, fields) {
                    if (error) {
                        console.log("事务[领橘子]第二步(修改用户余额)失败, 回滚吧")
                        return connection.rollback(function () {
                            callback_error(error)
                            throw error;
                        });
                    }
                    // record 消费记录添加一条数据
                    connection.query("INSERT INTO record (openid, rid, from_openid, type, des, balance_orange, balance_money, differ_orange) values(?,?,?,?,?,?,?,?)", [recordObj.openid, recordObj.rid, recordObj.from_openid, 5, "领取橘子", recordObj.balance_orange, recordObj.balance_money, 1], function (error, results, fields) {
                        if (error) {
                            console.log("事务[领橘子]第三步(添加消费记录)失败, 回滚吧")
                            return connection.rollback(function () {
                                callback_error(error)
                                throw error;
                            });
                        }

                        connection.commit(function (err) {
                            if (err) {
                                console.log("事务[领橘子]提交失败了, 有点意外!")
                                return connection.rollback(function () {
                                    callback_error(err)
                                    throw err;
                                });
                            }
                            callback_commit()
                            console.log('事务[领橘子]成功 - success!');
                            connection.end()
                        });

                    })

                });
            });
        });
    },
    /**
     * 发橘子事务
     */
    doTranStartRound: function (openid, amount, slogen, new_orange, balance_money, callback_error, callback_commit) {
        var connection = mysql.createConnection(db_config);//创建连接
        var rid = -1;
        //开始处理事务
        connection.beginTransaction(function (err) {
            if (err) {
                throw err;
                console.log("开启事务[发橘子]失败")
            }
            //扣掉用户橘子余额
            connection.query("UPDATE user SET orange=? WHERE openid=?", [new_orange, openid], function (error, results, fields) {
                if (error) {
                    console.log("发橘子:事务第一步失败, 回滚吧")
                    return connection.rollback(function () {
                        callback_error(error)
                        throw error;
                    });
                }
                //添加round数据
                connection.query("INSERT INTO round(uid, total, slogen) VALUES(?, ?, ?)", [openid, amount, slogen], function (error, results, fields) {
                    if (error) {
                        console.log("发橘子:事务第二步失败, 回滚吧")
                        return connection.rollback(function () {
                            callback_error(error)
                            throw error;
                        });
                    }
                    rid = results.insertId;//新创建的游戏id
                    //添加record记录
                    connection.query("INSERT INTO record(openid, rid, type, des, balance_orange, balance_money, differ_orange) VALUES(?, ?, ?, ?, ?, ?, ?)", [openid, rid, 2, "分享橘子给好友", new_orange, balance_money, amount], function (error, results, fields) {
                        if (error) {
                            console.log("发橘子: 事务第三步[添加recrod记录]失败, 回滚")
                            return connection.rollback(function () {
                                callback_error(error)
                                throw  error;
                            })
                        }
                        //事务步骤完成, 提交!!!!
                        connection.commit(function (error) {
                            if (error) {
                                console.log("发橘子: 事务commit失败, 回滚吧")
                                return connection.rollback(function () {
                                    callback_error(error)
                                    throw error
                                })
                            }
                            callback_commit(rid)
                            console.log('发橘子:事务成功 - success!');
                            connection.end()
                        })
                    })
                })
            })
        });
    }
}

