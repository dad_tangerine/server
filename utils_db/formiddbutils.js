var dbUtils = require('./db')

module.exports={
    addFormid: function (openid, formid, callback) {

        dbUtils.query("INSERT INTO formid(openid, formid) VALUES(?, ?)", [openid, formid], function (results, fields) {
            callback(results, fields)
        })
    },
    removeFormid: function (id, callback) {
        dbUtils.query("DELETE FROM formid WHERE id=?", id, function (results, fields) {
            callback(results, fields)
        })
    },
    getFromidByOpenid: function (openid, callback) {
        dbUtils.query("SELECT * FROM formid WHERE openid=? limit 1", openid, function (results, fields) {
            callback(results, fields)
        })
    }
}