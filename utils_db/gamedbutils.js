var dbutils = require('./db.js')

module.exports = {

    /**
     * 根据openid获取用户信息
     * @param uid  openid
     * @param total  橘子总数
     * @param slogen  口号
     * @param callback
     */
    createRound: function(uid, total, slogen, callback){
        dbutils.query("INSERT INTO round(uid, total, slogen) VALUES(?,?,?)", [uid, total, slogen], function (results, fields) {
            callback(results, fields)
        })
    },
    getGameByGid: function (gid, callback) {
        dbutils.query("SELECT * FROM round where id=?", [gid], function (results, fields) {
            callback(results, fields)
        })
    },
    getEnableGameByGid: function (gid, callback) {
        dbutils.query("SELECT * FROM round where id=? and state=0", [gid], function (results, fields) {
            callback(results, fields)
        })
    },
    getRoundsByOpenid: function (openid, callback) {
        dbutils.query("SELECT * FROM round where uid = ?", [openid], function (results, fields) {
            callback(results, fields)
        })
    },
    addReceived: function(rid, callback){
        // dbutils
    },
    get_beenreturn: function (callback) {
        dbutils.query("SELECT * FROM round WHERE state !=?", 3, function(results, fields){
            callback(results, fields)
        })
    },
    setRoundDisable: function (id, callback) {
        dbutils.query("UPDATE round SET state = 3 WHERE id=?", id, function (results, fields) {
            callback(results, fields)
        })
    }
}