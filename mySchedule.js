var schedule = require('node-schedule');
var async_npm = require('async');

var myConfig = require('./config');

var userDbUtils = require('./utils_db/userdbutils');
var gameDbUtils = require('./utils_db/gamedbutils');
var scheduleDbUtils = require('./utils_db/scheduledbutils');
var partakeDbUtils = require('./utils_db/partakedbutils');

var redisHelp = require('./utils_db/redis_help');

var getAccessTokenUtils = require('./utils_http/getAccessToken');
var sendTMsgUtils = require('./utils_http/sendTMsgUtils');

var differ_time = 24 * 60 * 60 * 1000;
/**
 * 超时返还
 * 每一个小时清算一次超时橘子返还
 */
async function return_orange_schedule() {
    schedule.scheduleJob('0 0 * * * *', function () {
        var now_date = new Date();
        // var differ_time = 24 * 60 * 60 * 1000;

        console.log("[执行24小时清算:开始]:", now_date);
        var roundList = null;

        gameDbUtils.get_beenreturn(function (results, fields) {
            roundList = results;
            console.log("[执行24小时清算]:共有", results.length, "局游戏需要清算!");

            task(roundList, now_date)
                .then(function () {
                    console.log("[执行24小时清算:本次任务完成]>>>>>>>>>>>>>>>")
                })
                .catch((err) => {
                    if (err) {
                        console.log("[执行24小时清算:异步失败]:", err)
                    }
                });
        })
    });
}

async function task(roundList, now_date) {
    for (let i = 0; i < roundList.length; i++) {
        let value = roundList[i];
        if (now_date.getTime() - new Date(value.time).getTime() > differ_time) {
            var CURRENT_partakeList = await promise_getPartakesById(value.id);

            if (CURRENT_partakeList.length === parseInt(value.total)) {//已经被领完了,直接将round的状态state设置为3
                await promise_setRoundDisable(value.id);
            } else {//没被领完, 开始返还
                var CURRENT_USER = await promise_getUserInfoByOpenid(value.uid);
                if (CURRENT_USER == null) {
                    console.log("[执行24小时清算:被返还用户未找到]");
                    return;
                }
                //计算出要返还的橘子数量
                var differ_orange = parseInt(value.total) - CURRENT_partakeList.length;
                console.log("^^^^^^^^^^^^^^^^^^^^^^本次清算钱的账户余额为:", CURRENT_USER.orange, "要返还多少个:", differ_orange);

                var result = await promise_return_orange(CURRENT_USER, value, differ_orange);
                if (result) {
                    //本次结束
                }
            }
        } else {
            console.log("[执行24小时清算:此局游戏尚未结束,不执行清算]游戏id:", value.id);
        }
    }
}

/**
 * 定时获取access_token
 */
function getAccessToken_schedule() {
    schedule.scheduleJob('0 0 * * * *', function () {
        console.log("[执行1小时获取accessToken任务开始:]", new Date());
        doGetAccessToken()
    })
}

function doGetAccessToken() {
    getAccessTokenUtils.getAccessToken(function (data) {
        data = JSON.parse(data);
        if (!data.errcode) {
            redisHelp.set("access_token", data.access_token);
            console.log("[执行1小时获取accessToken任务:成功]", data.access_token)
        } else {
            console.log("获取accessToken失败啦")
        }
    }, function (error) {
        doGetAccessToken();
        console.log("获取accessToken: 网络失败");
    })
}


async function promise_getPartakesById(game_id) {
    return new Promise(function (resolve, reject) {
        partakeDbUtils.getPartakesByRid(game_id, function (results, fields) {
            resolve(results, fields)
        })
    })
}

async function promise_setRoundDisable(game_id) {
    return new Promise(function (resolve, reject) {
        gameDbUtils.setRoundDisable(game_id, function (results, fields) {
            console.log("[执行24小时清算:此局游戏清算成功(无需返还)]游戏id:", game_id);
            resolve(true);
        })
    })
}

async function promise_getUserInfoByOpenid(openid) {
    //获取被返还的用户
    return new Promise(function (resolve, reject) {
        userDbUtils.getUserInfoByOpenid(openid, function (results, fields) {
            resolve(results[0])
        })
    })
}

async function promise_return_orange(CURRENT_USER, CURRENT_ROUND, differ_orange) {
    return new Promise(function (resolve, reject) {
        scheduleDbUtils.return_orange(CURRENT_USER, CURRENT_ROUND, differ_orange, function (error) {
            console.log("[执行24小时清算:此局游戏清算失败]游戏id:", value.id, "\n   错误原因:", error)
        }, function () {
            console.log("[执行24小时清算:此局游戏清算成功]游戏id:", CURRENT_ROUND.id);
            //发模板消息通知用户: 您的橘子超过24小时被退还了
            sendTMsgUtils.doMSG_return(CURRENT_USER.openid);
            resolve()
        })
    })
}

return_orange_schedule();
getAccessToken_schedule();