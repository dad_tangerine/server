var https = require('https')
var qs = require('querystring')
var config = require('../config.js')


module.exports.getSession = function(code, callback_data, callback_error){
    //这里主要是为了拿到openid,sessionkey
    var options = {
        hostname: 'api.weixin.qq.com',
        port: '443',
        path: '/sns/jscode2session?appid=' + config.appid + '&secret=' + config.secret + '&js_code=' + code + '&grant_type=authorization_code',
        method: 'GET'
    };
    var req = https.request(options, function (res) {
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            callback_data(chunk)
        });
        /**
         * 返回值包括: openid, session_key, unionid, errcode, errMsg
         * errorcode: 0:成功, -1:系统繁忙, 40029:code无效, 45011:频率限制，每个用户每分钟100次
         */
    });

    req.on('error', function (e) {
        callback_error(e);
    });

    req.end();
}