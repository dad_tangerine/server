var https = require('https')
var qs = require('querystring')
var config = require('../config.js')

/**
 * 统一下单
 * @param appid
 * @param mch_id
 * @param nonce_str 随机字符串
 * @param sign 签名
 * @param body 商品描述
 * @param detail 商品详情
 * @param attach 附加数据
 * @param out_trade_no 商户订单号
 * @param total_fee 标价金额
 * @param spbill_create_ip 终端IP
 * @param notify_url 通知地址
 * @param trade_type 交易类型
 * @param callback_data
 * @param callback_error
 */
// module.exports.pay = function (appid, mch_id, nonce_str, sign, body, detail, attach, out_trade_no, total_fee, spbill_create_ip, notify_url, trade_type, callback_data, callback_error) {
module.exports.pay = function (post_xml, callback_data, callback_error) {

    var options = {
        hostname: 'api.mch.weixin.qq.com',
        port: '443',
        path: '/pay/unifiedorder',
        method: 'POST'
    };
    var req = https.request(options, function (res) {
        // console.log('STATUS: ' + res.statusCode);
        // console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            callback_data(chunk)
        });
    });

    req.on('error', function (e) {
        callback_error(e);
    });

    req.write(post_xml)
    req.end();
}