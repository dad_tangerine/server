var https = require('https')
var qs = require('querystring')
var config = require('../config.js')

module.exports = {
    getAccessToken: function (callback_data, callback_error) {
        var options = {
            hostname: 'api.weixin.qq.com',
            port: '443',
            path: '/cgi-bin/token?grant_type=client_credential&appid=' + config.appid + '&secret=' + config.secret,
            method: 'GET'
        };
        var req = https.request(options, function (res) {
            // console.log('STATUS: ' + res.statusCode);
            // console.log('HEADERS: ' + JSON.stringify(res.headers));
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                callback_data(chunk)
            });
        });

        req.on('error', function (e) {
            callback_error(e);
        });

        // req.write(qs.stringify(dataJson))
        req.end();
    }
}