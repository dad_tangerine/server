var myConfig = require('../config')
var sendTempMsg = require('./sendTempMsg')

var formidDbUtils = require('../utils_db/formiddbutils')
var redisHelp = require('../utils_db/redis_help')
var timeUtils = require('../utils_common/timeutils')

var formTempId_return = "Uv5rrAE7mgnYOqataZXw2O_EbeUC1kKU-So9Rh7HjNI"//24小时退还通知
var formTempId_end = "li3sXckPbBtuvCHsUi9qr22zkYZ5snk_djPUeKm890s"//橘子被领完了
var formTempId_getsuccess = "faCXFKIJY7mazQq7E7g1v9J98Tvx1kVoh40_hJSYs4Y"//领取成功通知
var fromTempId_sendsuccess = "H_ZUxSwtLIzMFC-6v9rpv7zFZzNLvczoXwoZ9M_geKk"//发送橘子成功通知

module.exports = {
    /**
     * 发送24小时退货模板消息
     * @param openid
     */
    doMSG_return: function (openid) {
        var dataJson = {}
        var access_token = "0"
        var CURRENT_FORMID = null
        redisHelp.get("access_token", function (error, replay) {
            if (error) {
                console.log("[模板消息: 退还通知]失败!=>>access_token获取失败, openid=", openid)
                return;
            }
            if (replay == null) {
                console.log("[模板消息: 退还通知]失败!=>>access_token获取失败=null, openid=", openid)
                return
            }
            access_token = replay

            formidDbUtils.getFromidByOpenid(openid, function (results, fields) {
                if (results.length != 1) {
                    console.log("[模板消息: 退还通知]失败!=>>缺少formid, openid=", openid)
                    return
                }
                //拿到当前的 fromid 数据
                CURRENT_FORMID = results[0]
                //删除用过的formid
                formidDbUtils.removeFormid(CURRENT_FORMID.id, function (results, fileds) {
                })

                dataJson = {
                    "touser": openid,
                    "template_id": formTempId_return,
                    // "page": page,
                    "form_id": CURRENT_FORMID.formid,
                    "page": "pages/index/index",
                    "data": {
                        "keyword1": {
                            "value": "橘子"
                        },
                        "keyword2": {
                            "value": "您的橘子24小时内未领完，已退回至您的账户"
                        }
                    }
                    // "emphasis_keyword": "keyword1.DATA"//需要放大的关键词
                }
                sendTempMsg.sendTempMsg(access_token, dataJson, function (data) {
                    if (data.errcode == 0) {
                        console.log("[模板消息: 退还通知]SUCCESS!用户openid=", openid)
                    }
                    console.log("[模板消息: 退还通知]最后的数据是=>>" + data)
                }, function (error) {
                    console.log("[模板消息: 退还通知]ERROR!openid=", openid)
                })
            })
        })
    },
    /**
     * 橘子被领完的模板消息
     * @param openid
     */
    doMSG_end: function (openid, rid) {
        var dataJson = {}
        var access_token = myConfig.access_token
        var CURRENT_FORMID = null
        redisHelp.get("access_token", function (error, replay) {
            if (error) {
                console.log("[模板消息: 橘子被领完]失败!=>>access_token获取失败, openid=", openid)
                return;
            }
            if (replay == null) {
                console.log("[模板消息: 橘子被领完]失败!=>>access_token获取失败=null, openid=", openid)
                return
            }
            access_token = replay

            formidDbUtils.getFromidByOpenid(openid, function (results, fields) {
                if (results.length != 1) {
                    console.log("[模板消息: 橘子被领完]失败!=>>缺少formid, openid=", openid)
                    return
                }
                //拿到当前的 fromid 数据
                CURRENT_FORMID = results[0]
                //删除用过的formid
                formidDbUtils.removeFormid(CURRENT_FORMID.id, function (results, fileds) {
                })

                dataJson = {
                    'touser': openid,
                    'template_id': formTempId_end + '',
                    'form_id': CURRENT_FORMID.formid + '',
                    "page": "pages/result/result?gameId=" + rid + "&source=template",
                    'data': {
                        'keyword1': {
                            "value": "橘子"
                        },
                        'keyword2': {
                            "value": '您的橘子被好友领完啦!'
                        }
                    }
                    // "emphasis_keyword": "keyword1.DATA"//需要放大的关键词
                }
                sendTempMsg.sendTempMsg(access_token, dataJson, function (data) {
                    if (data.errcode == 0) {
                        console.log("[模板消息: 橘子被领完]SUCCESS!用户openid=", openid)
                    }
                    console.log("[模板消息: 橘子被领完]最后的数据是=>>" + data)
                }, function (error) {
                    console.log("[模板消息: 橘子被领完]ERROR!用户openid=", openid)
                })
            })
        })
    },
    /**
     * 橘子领取成功的通知
     * @param openid
     */
    doMSG_getsuccess: function (openid) {
        console.log("调用了橘子领取成功过模板消息^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        var dataJson = {}
        var access_token = myConfig.access_token
        var CURRENT_FORMID = null
        redisHelp.get("access_token", function (error, replay) {
            if (error) {
                console.log("[模板消息: 橘子领取成功]失败!=>>access_token获取失败, openid=", openid)
                return;
            }
            if (replay == null) {
                console.log("[模板消息: 橘子领取成功]失败!=>>access_token获取失败=null, openid=", openid)
                return
            }
            access_token = replay
            console.log("调用了橘子领取成功过模板消息^^^^^^^^^^^^拿到access_token:", replay)
            formidDbUtils.getFromidByOpenid(openid, function (results, fields) {
                if (results.length != 1) {
                    console.log("[模板消息: 橘子领取成功]失败!=>>缺少formid, openid=", openid)
                    return
                }
                //拿到当前的 fromid 数据
                CURRENT_FORMID = results[0]
                console.log("调用了橘子领取成功过模板消息^^^^^^^^^^^^拿到formid:", CURRENT_FORMID)
                //删除用过的formid
                formidDbUtils.removeFormid(CURRENT_FORMID.id, function (results, fileds) {
                    console.log("调用了橘子领取成功过模板消息^^^^^^^^^^^^删除formid成功")
                })

                dataJson = {
                    "touser": openid,
                    "template_id": formTempId_getsuccess,
                    // "page": page,
                    "form_id": CURRENT_FORMID.formid,
                    "data": {
                        "keyword1": {
                            "value": "橘子"
                        },
                        "keyword2": {
                            "value": "您成功领取到一个橘子!"
                        }
                    }
                    // "emphasis_keyword": "keyword1.DATA"//需要放大的关键词
                }
                console.log("调用了橘子领取成功过模板消息^^^^^^^^^^^^准备调用请求")
                sendTempMsg.sendTempMsg(access_token, dataJson, function (data) {
                    if (data.errcode == 0) {
                        console.log("[模板消息: 橘子领取成功]SUCCESS!用户openid=", openid)
                    }
                    console.log("[模板消息: 橘子领取成功]最后的数据是=>>" + data)
                }, function (error) {
                    console.log("[模板消息: 橘子领取成功]ERROR!用户openid=", openid)
                })
            })
        })
    },
    /**
     * 发橘子成功通知
     * @param openid
     */
    doMSG_sendsuccess: function (openid, rid, uname, gifenable) {
        var dataJson = {}
        var access_token = myConfig.access_token
        var CURRENT_FORMID = null

        var page = "pages/result/result?gameId=" + rid

        // if (gifenable) {
        //     page = "pages/videoPage/videoPage?gameId=" + rid + "&shareOpenId=" + openid + "&source=template"
        // } else {
        //     page = "pages/sharePage/sharePage?gameId=" + rid + "&shareOpenId=" + openid
        // }

        redisHelp.get("access_token", function (error, replay) {
            if (error) {
                console.log("[模板消息: 发出去橘子]失败!=>>access_token获取失败, openid=", openid)
                return;
            }
            if (replay == null) {
                console.log("[模板消息: 发出去橘子]失败!=>>access_token获取失败=null, openid=", openid)
                return
            }
            access_token = replay

            formidDbUtils.getFromidByOpenid(openid, function (results, fields) {
                if (results.length != 1) {
                    console.log("[模板消息: 发出去橘子]失败!=>>缺少formid, openid=", openid)
                    return
                }
                //拿到当前的 fromid 数据
                CURRENT_FORMID = results[0]
                //删除用过的formid
                formidDbUtils.removeFormid(CURRENT_FORMID.id, function (results, fileds) {
                })

                dataJson = {
                    "touser": openid,
                    "template_id": fromTempId_sendsuccess,
                    "page": page,
                    "form_id": CURRENT_FORMID.formid,
                    "data": {
                        "keyword1": {
                            "value": "橘子"
                        },
                        "keyword2": {
                            "value": uname
                        },
                        "keyword3": {
                            "value": timeUtils.getDateToString2(new Date())
                        },
                        "keyword4": {
                            "value": "橘子发布成功，快分享给好友吧～"
                        }
                    }
                    // "emphasis_keyword": "keyword1.DATA"//需要放大的关键词
                }
                sendTempMsg.sendTempMsg(access_token, dataJson, function (data) {
                    if (data.errcode == 0) {
                        console.log("[模板消息: 发出去橘子]SUCCESS!用户openid=", openid)
                    }
                    console.log("[模板消息: 发出去橘子]最后的数据是=>>" + data)
                }, function (error) {
                    console.log("[模板消息: 发出去橘子]ERROR!openid=", openid)
                })
            })
        })
    }
}