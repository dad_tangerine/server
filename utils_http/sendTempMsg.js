var https = require('https')
var request = require('request')
var qs = require('querystring')
var config = require('../config.js')

module.exports = {
    /**
     * 发送模板消息 http方法
     * @param access_token
     * @param touser 接受用户的openid
     * @param template_id 模板id
     * @param page 此消息的跳转地址
     * @param form_id 用户的formid
     * @param data 模板内容，不填则下发空模板
     * @param callback_data
     * @param callback_error
     */
    sendTempMsg: function (access_token, params, callback_data, callback_error) {

        // console.log("^^^^^^^^^^^^^^^^^^^json数据为:" + JSON.stringify(params))

        var options = {
            hostname: 'api.weixin.qq.com',
            port: '443',
            path: '/cgi-bin/message/wxopen/template/send?access_token=' + access_token,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Length': content.length
            }
        }

        request({
            url: 'https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=' + access_token,
            method: "POST",
            json: true,
            body: params,
        }, function (error, res, body) {
            console.log("^^^^^^^^^^^^^^^^^^^^^==========^^^^^^^^^^^^^^^^^^");
            if(res.body.errorcode == 0 && res.body.errmsg == "ok"){
                callback_data(res.body)
            }else{
                callback_error(res.body)
            }
            console.log("模板消息-------finish")
            console.log("//////////////////////////////////////////////////")
        })

    }
}