var md5=require('md5')

module.exports = {
    /**
     * 将json数据按照ASIC码排序
     * @param ojb
     */
    sortJson: function (obj) {
        var new_obj = {}
        var keys = Object.keys(obj).sort()
        for (var i in keys) {
            var key = keys[i]
            new_obj[key] = obj[key]
        }
        return new_obj
    },
    /**
     * 拼接stringA
     * @param ojb
     * @returns {string}
     */
    get_stringA_Temp: function (obj, api_key) {
        var keys = Object.keys(obj)
        var stringA = ""
        for (var i in keys) {
            var key = keys[i]
            stringA += key + "=" + obj[key] + "&"
        }
        stringA += "key=" + api_key//加上密钥
        return stringA
    },
    get_sign: function (stringSignTemp) {
        return md5(stringSignTemp).toUpperCase()
    },
    /**
     * 将json数据组装成xml
     * @param obj
     * @param sign
     * @returns {string}
     */
    get_POST_XML: function (obj, sign) {
        // obj.sign = sign

        var post_xml = '<xml>' +
            '<appid>' + obj.appid + '</appid>' +
            '<attach>' + obj.attach + '</attach>' +
            '<body>' + obj.body + '</body>' +
            '<mch_id>' + obj.mch_id + '</mch_id>' +
            '<nonce_str>' + obj.nonce_str + '</nonce_str>' +
            '<notify_url>' + obj.notify_url + '</notify_url>' +
            '<openid>' + obj.openid + '</openid>' +
            '<out_trade_no>' + obj.out_trade_no + '</out_trade_no>' +
            '<spbill_create_ip>' + obj.spbill_create_ip + '</spbill_create_ip>' +
            '<total_fee>' + obj.total_fee + '</total_fee>' +
            '<trade_type>' + obj.trade_type + '</trade_type>' +
            '<sign>' + sign + '</sign>' +
            '</xml>';
        return post_xml
    }

}