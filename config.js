/**
 * 配置文件
 */

module.exports = {
    "verion": "1.0.0",
    // "appid":"wx1443885c9558e2f9",
    "appid":"wx1914d30c80460f6c",
    // "secret":"c9b76dbdca7ccf50dc020dbef84983fd",
    "secret":"5d2f5fee6b9108cec4616db8ddf641b8",
    // "mch_id":"1496523092",//商户号
    "mch_id":"1489306352",
    "notify_url":"https://orange.geinigejuzichi.top/wx_api/money/notify_pay_result",
    "api_key": "f1b23bc0e38711e89f32a5392df1ee4d",
    //数据库连接配置
    "db_config": {
        host: '127.0.0.1',
        user: 'root',
        // password: '48a18fb94a694',
        password: '1234qwer',
        database: 'orange'
    },
    'price_list': [
        {'price': 330, 'amonut': 3, 'msg':'', 'id':'111111'},
        {'price': 600, 'amonut': 6, 'msg':'', 'id':'222222'},
        {'price': 900, 'amonut': 10, 'msg':'', 'id':'333333'},
        {'price': 1800, 'amonut': 20, 'msg':'', 'id':'444444'},
        {'price': 3000, 'amonut': 33, 'msg':'', 'id':'555555'},
        {'price': 4500, 'amonut': 50, 'msg':'', 'id':'666666'},
    ],
    'gif':{
        'url': "http://orange.geinigejuzichi.top/orange.gif",
        'duration': 27000,
        'enable': true
    },
    "video_url": "http://orange.geinigejuzichi.top/orange_300k.mp4",
    "share_img_url": [
        "http://orange.geinigejuzichi.top/share_1.jpg",
        "http://orange.geinigejuzichi.top/share_2.jpg",
        "http://orange.geinigejuzichi.top/share_3.jpg",
        "http://orange.geinigejuzichi.top/share_4.jpg",
        "http://orange.geinigejuzichi.top/share_5.jpg"
    ],
    "share_msg": ["我买几个橘子去，你就在此地，不要走动～", "春运福利，车站的父爱～"],
    "result_page_enable": false,
    'calls': ['爸爸', 'baba', '粑粑', '老汉儿'],
    "orange_min": 50,
    "postage": 23,
    "postage_rule":"小于等于20个, 15元邮费, 大于20个每个1元",
    "access_token": "0"
}