module.exports = {

    /**
     * 展示发布者发布时间，规则如下：
     1. 小于60s，展示：刚刚
     2. 小于60分钟，展示：xx分钟前
     3. 小于24小时，展示：xx小时前
     4. 小于30天，展示：xx天前
     5. 大于30天，展示：x月x日
     6. 大于30天且跨年，展示：x年x月
     * @param timeStr
     */
    getTimeMsg: function (timeStr) {
        var myDate = new Date(timeStr)
        var nowDate = new Date()
        var differSec = parseInt((nowDate.getTime() - myDate.getTime()) / 1000)

        if (differSec < 60) {//小于60秒
            return "刚刚"
        } else if (differSec < 60 * 60) {//小于60分钟
            return parseInt(differSec / (60)) + "分钟前"
        } else if (differSec < 24 * 60 * 60) {//小于24小时
            return parseInt(differSec / (60 * 60)) + "小时前"
        } else if (differSec < 30 * 24 * 60 * 60) {//小于30天
            return parseInt(differSec / (24 * 60 * 60)) + "天前"
        } else {
            return (myDate.getMonth() + 1) + "月" + myDate.getDate() + "日"
        }
    },
    /**
     * 获取时间戳
     * 从 1970 年 1 月 1 日 00:00:00 至今的秒数，即当前的时间
     */
    getTimeStamp: function () {
        return parseInt(new Date().getTime() / 1000)
    },
    getDateToString: function (params) {
        var date = new Date(params)
        return date.getFullYear() + "年" + (date.getMonth() + 1) + "月" + date.getDate() + " " + date.getHours() + "时" + date.getMinutes() + "分" + date.getSeconds() + "秒";
    },
    getDateToString2: function (date) {
        return date.getFullYear() + "年" + (date.getMonth() + 1) + "月" + date.getDate() + " " + date.getHours() + "时" + date.getMinutes() + "分";
    },
    getDateToStringYYYYMMDD: function(param){
        var date = new Date(param)
        var year = date.getFullYear()
        var month = date.getMonth()
        var day = date.getDate()
        if(month<10){
            month ="0" + month
        }
        if(day<10){
            day = "0" + day
        }
        return year+""+month+""+day
    }
}