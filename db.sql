CREATE DATABASE orange;

#创建用户表
CREATE TABLE user(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    openid VARCHAR(100) NOT NULL,
    unionid VARCHAR(100) NOT NULL DEFAULT '0000',
    channel VARCHAR(100) NOT NULL DEFAULT "orange",
    nickName VARCHAR(100),
    avatarUrl VARCHAR(500),
    gender INT DEFAULT 1,#1:男,2:女
    addr VARCHAR(512),#地址
    balance INT DEFAULT 0,#余额,单位为分
    orange INT DEFAULT 2,#账户橘子数量
    time TIMESTAMP DEFAULT current_timestamp
);

#创建一句游戏表
CREATE TABLE round(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    uid VARCHAR(100) NOT NULL,#创建游戏的人,这里是openid
    total INT NOT NULL,#总个数
    received INT DEFAULT 0,#已经被领取的个数
    time TIMESTAMP DEFAULT current_timestamp,
    state INT DEFAULT 0,#0: 可领取, 1:领完, 2:超时不可领取, 3:已经24小时清算
    slogen VARCHAR(100)
);

#加入游戏表
CREATE TABLE partake(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    rid INT NOT NULL,
    uid VARCHAR(100) NOT NULL,
    subject VARCHAR(100) NOT NULL DEFAULT "baba",
    time TIMESTAMP DEFAULT current_timestamp #领取时间
);

#消费记录表
CREATE TABLE record(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    openid VARCHAR(100) NOT NULL,
    rid INT DEFAULT -1,#游戏id, 只有当type=5时有效
    from_openid VARCHAR(100) DEFAULT "-1",#被领取人的id, 只有当type=5时有效
    type INT NOT NULL,# 1:充值,2:发出去,3:提现,4:购买橘子,5:叫爸爸得到橘子, 6: 超时返还橘子
    differ_orange INT NOT NULL DEFAULT 0,
    differ_money INT NOT NULL DEFAULT 0,
    des VARCHAR(100) NOT NULL,
    balance_orange INT NOT NULL,
    balance_money INT NOT NULL,
    time TIMESTAMP DEFAULT current_timestamp
);

#充值订单表
CREATE TABLE trade(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    time TIMESTAMP DEFAULT current_timestamp,
    openid VARCHAR(100) NOT NULL,
    out_trade_no VARCHAR(100) NOT NULL,#商户订单号
    total_fee INT NOT NULL,#充值金额,单位为分
    orange INT NOT NULL,#橘子的个数
    detail VARCHAR(500) NOT NULL,#订单描述
    state INT DEFAULT 0,#为0时, 标示未完成, 1表示已经支付成功
    prepay_id VARCHAR(100) NOT NULL,#预付码
    nonce_str VARCHAR(100) NOT NULL,#随机数串
    finish_time VARCHAR(100) DEFAULT "0000"#订单完成时间
);

#提现表
CREATE TABLE withdraw(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    time TIMESTAMP DEFAULT current_timestamp,
    type INT NOT NULL DEFAULT 0,#0: 包邮, 1: 购满60个, 2: 自付邮费
    openid VARCHAR(100) NOT NULL,
    orange_number INT DEFAULT 0,
    orange_weight INT DEFAULT 0,#单位: 克
    money INT DEFAULT 0,#需要支付的费用
    orange INT DEFAULT 0,#需要支付的橘子数量
    payment INT DEFAULT 0,#0: 未支付邮费, 1: 已经支付邮费(或者包邮)
    addr VARCHAR(500) NOT NULL,#收货地址
    tel VARCHAR(100) NOT NULL,#电话
    consignee VARCHAR(100) NOT NULL,#收件人
    state INT DEFAULT 0 #0: 未处理, 1: 未付邮费, 2: 未发货, 3: 已经发货, -1: 订单取消
);

#formid表
CREATE TABLE formid(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    openid VARCHAR(100) NOT NULL,
    formid VARCHAR(100) NOT NULL
);