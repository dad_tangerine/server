var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);

require('./mySchedule.js')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var gameRouter = require('./routes/game');
var partakeRouter = require('./routes/partake')
var moneyRouter = require('./routes/money')
var recordRouter = require('./routes/record')
var withdrawRouter = require('./routes/withdraw')

var adminIndexRouter = require('./admin/index')
var adminChannelRouter = require('./admin/channel')
var adminOverviewRouter = require('./admin/overview')
var adminConsumeRouter = require('./admin/consume')
var adminOrderRouter = require('./admin/order')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(bodyParser.json())
app.use(bodyParser.xml())
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/wx_api/user', usersRouter);
app.use('/wx_api/game', gameRouter);
app.use('/wx_api/partake', partakeRouter)
app.use('/wx_api/money', moneyRouter)
app.use('/wx_api/record', recordRouter)
app.use('/wx_api/withdraw', withdrawRouter)

app.use('/admin', adminIndexRouter)
app.use('/admin', adminChannelRouter)
app.use('/admin', adminOverviewRouter)
app.use('/admin', adminConsumeRouter)
app.use('/admin', adminOrderRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
app.listen(3000)

module.exports = app;
