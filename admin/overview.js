var express = require('express');
var router = express.Router();

var overviewDbUtils = require('../utils_db/admin/overview')


/**
 * 总览
 */
router.get('/overview', async function(req, res, next){
    try {
        var results = await overviewDbUtils.totalUsers()
        res.render("overview", {
            result: 0,
            data: {
                total: results[0].total
            }
        })
        return
    }catch (error){
        console.log(error)
        res.render("error",{
            result: -1,
            msg: error
        })
        return
    }

})

router.post('/getUsers', async function (req, res, next) {
    var startTime = req.query.start_time
    var endTime = req.query.end_time
    var page = req.query.page
    var size = req.query.size

    if(startTime == undefined || startTime == ""){
        startTime = new Date(0)
    }else{
        startTime = new Date(startTime)
    }
    if(endTime == undefined || endTime == ""){
        endTime = new Date()
    }else{
        endTime = new Date(endTime)
    }
    if(page == undefined){
        page = 0
    }
    if(size == undefined){
        size = 20
    }
    console.log("startTime: ", startTime, ", endTime: ", endTime)
    try {
        var results = await overviewDbUtils.findUsers(page, size, startTime, endTime);
        console.log("数据条数: ", results.length)
        res.send({
            result: 0,
            data: results
        })
        return
    }catch (error){
        res.send({
            result: -1,
            msg: error
        })
        return
    }

})


module.exports = router