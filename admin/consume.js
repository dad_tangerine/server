var express = require('express');
var router = express.Router();

var consumeDbUtils = require('../utils_db/admin/consume')


/**
 * 总览
 */
router.get('/getConsume', async function(req, res, next){

    var openid = req.query.openid
    if(openid == undefined || openid == ""){
        res.render("consume", {
            error: -1,
            'msg': '参数错误'
        })
        return
    }

    try {
        var results = await consumeDbUtils.getRecordByOpenid(openid)
        for(var i=0; i<results.length; i++){
            results[i].type = setType2Msg(results[i].type)
        }
        res.render("consume", {
            error: 0,
            data: results
        })
        return
    }catch (error){
        console.log(error)
        res.render("consume", {
            error: 0,
            'msg': "系统错误"
        })
        return
    }

})

function setType2Msg(type){
    var typeStr = "未知类型"
    if(type == 1){
        typeStr = "充值"
    }else if(type == 2){
        typeStr = "发出去"
    }else if(type == 3){
        typeStr = "提现"
    }else if(type == 4){
        typeStr = "购买橘子"
    }else if(type == 5){
        typeStr = "叫爸爸得到橘子"
    }else if(type == 6){
        typeStr = "超时返还橘子"
    }
    return typeStr
}


module.exports = router