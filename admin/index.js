var express = require('express');
var router = express.Router();
var async = require('async')
var Promise = require('promise');

var userDbUtils = require('../utils_db/userdbutils')
var adminUserDb = require('../utils_db/admin/userdbutils')

router.get('/index', function (req, res, next) {

    /**

     返回值:
     [

     {
        channel, total, recharge, profile
     }........
     total_all, recharge_all, profile_all
     ]

     */
    var data = {};
    var dataList = [];
    var channelList;
    var totalAll = 0;
    var rechargeAll = 0;
    var profileAll = 0;
    var rechargeTimesAll = 0;
    userDbUtils.getChannelTotal(function (results, fields) {
        channelList = results
        console.log("多少个渠道:", channelList.length)
        async.forEachOf(channelList, function (value, key, callback) {
            //根据渠道名查到所有消费记录
            adminUserDb.getRecordByChannel(value.channel, function (results, fields) {
                var recordList = results
                console.log('渠道:', value.channel, "共查到消费记录:", recordList.length)
                console.log("其中一条消费记录拿出来看看:", recordList[0])
                totalAll += recordList.length
                var recharge = 0;
                var profile = 0;
                for (var i = 0; i < recordList.length; i++) {
                    recharge += recordList[i].differ_money/100
                    profile += recordList[i].differ_money/100 * 0.2
                }
                //汇总
                rechargeAll += recharge;
                profileAll += profile
                rechargeTimesAll += recordList.length
                dataList.push({
                    channel: value.channel,
                    total: value.total,
                    recharge: recharge,
                    profile: profile.toFixed(2),
                    recharge_times: recordList.length
                })
                callback();
            })
        }, function (error) {
            if (error) {
                res.render('index', {
                    result: -1,
                })
                return;
            }
            data.total_all = totalAll
            data.recharge_all = rechargeAll
            data.profile_all = profileAll.toFixed(2)
            data.recharge_times_all = rechargeTimesAll
            data.list = dataList

            res.render('index', {
                result: 0,
                data: data
            })
            console.log("我要看看主页的数据:\n", data)
            return;
        })

    })


})


module.exports = router;