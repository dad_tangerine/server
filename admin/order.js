var express = require('express');
var router = express.Router();

var orderDbUtils = require('../utils_db/admin/order')

var timeUtils = require('../utils_common/timeutils')


/**
 * 提现总览列表
 */
router.get('/order', async function(req, res, next){
    try {
        var results = await orderDbUtils.getOrdersAll()
        for(var i=0; i<results.length; i++){
            results[i].type = setWithdraw_type(results[i].type);
            results[i].state = setWithdraw_state(results[i].state);
            results[i].payment = setWithdraw_payment(results[i].payment);
            results[i].time = timeUtils.getDateToString(results[i].time);
        }
        res.render("order", {
            result: 0,
            data: results
        })
        return
    }catch (error){
        console.log(error)
        res.render("error",{
            result: -1,
            msg: error
        })
        return
    }

})


router.post('updateState', async function (req, res, next) {
    var id = req.body.id;
    var state = req.body.state;

    if(id == undefined || state == undefined){
        res.send({
            error: -1,
            msg: "参数错误"
        })
        return
    }

    try {
        var results = await orderDbUtils.updateOrder(id, state)
        console.log(results)
        res.send({
            result: 0
        })
    }catch (error){
        res.send({
            result: -1,
            msg: error
        })
    }
})


function setWithdraw_type(type){
    var typeStr = "";
    switch(type){
        case 0:
            typeStr = "包邮"
            break;
        case 1:
            typeStr = "购满60个(十斤)"
            break;
        case 2:
            typeStr = "自付邮费"
            break;
        default:
            typeStr = "未知类型"
    }
    return typeStr;
}

function setWithdraw_state(state){
    var stateStr = "";
    switch(state){
        case -1:
            stateStr = "订单取消"
            break;
        case 0:
            stateStr = "未处理"
            break;
        case 1:
            stateStr = "未付邮费"
            break;
        case 2:
            stateStr = "未发货"
            break;
        case 3:
            stateStr = "已发货"
            break;
        default:
            stateStr = "未知状态"
    }
    return stateStr;
}

function setWithdraw_payment(payment){
    var paymentStr = "";
    switch(payment){
        case 0:
            paymentStr = "未支付邮费"
            break;
        case 1:
            paymentStr = "已支付邮费"
            break;
        default:
            paymentStr = "未知邮费状态"
    }
    return paymentStr;
}


module.exports = router