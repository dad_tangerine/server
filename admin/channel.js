var express = require('express');
var router = express.Router();


var adminUserDb = require('../utils_db/admin/userdbutils')
var timeUtils = require('../utils_common/timeutils')

router.get('/channel', function (req, res, next) {
    /**
     返回值:{
        recharge_all,
        profile_all,
        list:[
            { id, openid, recharge, profile, data}
        ]
     }

     */
    var channel = req.query.channel//渠道名称
    var startTime = req.query.start_time
    var endTime = req.query.end_time

    if (channel == undefined) {
        console.log("进入代理商详情页错误: 重定向到首页")
        res.redirect(302, '/admin/index')
        return
    }
    if (startTime == undefined || endTime == undefined) {
        startTime = new Date(0)
        endTime = new Date()
    }
    startTime = new Date(startTime)
    endTime = new Date(endTime)
    console.log("代理商详情页参数", channel, "-", startTime, "-", endTime)

    //TODO:/总用户数, 充值总数, 充值详情, 利润总数, 利润详情
    var rechargeAll = 0;
    var profileAll = 0;
    var dataList = []
    var data = {}
    adminUserDb.getRecordByChannel(channel, function (results, fileds) {
        for (var i = 0; i < results.length; i++) {
            var item = results[i]
            var recharge = item.differ_money / 100;
            var profile = recharge * 0.2 / 100
            rechargeAll += recharge
            profileAll += profile
            dataList.push({
                id: item.id,
                openid: item.openid,
                recharge: item.differ_money / 100,
                profile: item.differ_money * 0.2 / 100,
                time: timeUtils.getDateToString(item.time)
            })
        }
        data.recharge_all = rechargeAll
        data.profile_all = profileAll.toFixed(2)
        data.list = dataList

        res.render("channel", {
            'result': 0,
            'data': data
        })
    })


})


module.exports = router;