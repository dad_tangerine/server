var express = require('express');
var router = express.Router();

var gameDbUtils = require('../utils_db/gamedbutils')
var userDbUtils = require('../utils_db/userdbutils')
var partakeDbUtils = require('../utils_db/partakedbutils')
var recordDbUtils = require('../utils_db/recorddbutils')

var dbUtils = require('../utils_db/db')


var sendTMsgUtils = require('../utils_http/sendTMsgUtils')

/**
 * 领取橘子
 */
router.post('/pullFromRound', function (req, res, next) {
    var openid = req.body.openid;//领取用户的id
    var rid = parseInt(req.body.rid);//游戏id
    var call = req.body.call;//称呼

    if(call == ""){
        call = "BaBa"
    }

    console.log("领取橘子接口:", openid, "-", rid)

    if (openid == "" || rid == "") {
        res.send({
            'result': -1,
            'msg': "请传入正确参数uid, rid"
        })
        return
    }
    var CURRENT_USER = null;
    var CURRENT_ROUND = null;
    userDbUtils.getUserInfoByOpenid(openid, function (results, fields) {
        if (results.length == 0) {
            res.send({
                'result': -2,
                'msg': '查无此人, 无法领取!'
            })
            console.log("ERROR -- 查无此人, 无法领取!")
            return
        }
        CURRENT_USER = results[0]//获取当前用户
        //获取旧的橘子余额
        orangeOld = parseInt(results[0].orange);
        gameDbUtils.getEnableGameByGid(rid, function (results, fields) {
            if (results.length == 0) {
                res.send({
                    'result': -3,
                    'msg': '查无此局游戏或者超时无法领取'
                })
                console.log("ERROR -- 查无此局游戏!")
                return
            }
            CURRENT_ROUND = results[0]
            if(CURRENT_ROUND.uid == openid){
                res.send({
                    'result': -6,
                    'msg': '不能领取自己的橘子!'
                })
                console.log("ERROR -- 不能领取自己的橘子!")
                return
            }
            if ((new Date().getTime() - new Date(CURRENT_ROUND.time).getTime()) > 86400000) {
                res.send({
                    'result': -5,
                    'msg': '超过24小时无法领取'
                })
                return
            }
            //查看这局游戏共有多少参与者
            var sendPullFinishMsg = false
            partakeDbUtils.getPartakesByRid(CURRENT_ROUND.id, function (results, fileds) {
                if (parseInt(results.length) >= parseInt(CURRENT_ROUND.total)) {

                    res.send({
                        'result': -4,
                        'msg': '来晚了,橘子已经领完!'
                    })
                    console.log("ERROR -- 来晚了,橘子已经领完!")
                    return
                }
                for(var j=0; j<results.length; j++){
                    console.log("来了来了:",results[j])
                    if(results[j].uid == openid){
                        res.send({
                            'result': -7,
                            'msg': '不能重复领取'
                        })
                        console.log("ERROR -- 不能重复领取!")
                        return
                    }
                }
                if(parseInt(results.length) + 1==parseInt(CURRENT_ROUND.total)){
                    sendPullFinishMsg = true
                }
                console.log("符合条件, 允许领取橘子一个")

                var recordObj = {
                    openid: openid,
                    rid: rid,
                    from_openid: CURRENT_ROUND.uid,
                    type: "5",
                    des: "领取橘子",
                    balance_orange: CURRENT_USER.orange + 1,
                    balance_money: CURRENT_USER.balance
                }
                //橘子有剩余, 可以领取, 并且开事务处理
                dbUtils.doTranscation(openid, rid, call, CURRENT_ROUND.uid, CURRENT_USER.orange + 1, recordObj, function (error) {
                    if (error) {//事务出错
                        res.send({
                            'results': -5,
                            'msg': '事务失败',
                            'data': "系统错误:无法领取!" + error
                        })
                        return
                    }

                }, function () {
                    res.send({
                        'result': 0,
                        'msg': "领取成功"
                    })
                    //被领完通知
                    if(sendPullFinishMsg){
                        sendTMsgUtils.doMSG_end(CURRENT_ROUND.uid, CURRENT_ROUND.id)
                    }
                    return
                })

            })
        })
    })

})

module.exports = router