var express = require('express');
var router = express.Router();

var async = require('async')

var myConfig = require('../config')

var dbUtils = require('../utils_db/db')
var gameDbUtils = require('../utils_db/gamedbutils')
var userDbUtils = require('../utils_db/userdbutils')
var partakeDbUtils = require('../utils_db/partakedbutils')

var timeUtils = require(('../utils_common/timeutils'))


var sendTMsgUtils = require('../utils_http/sendTMsgUtils')


/**
 * 发橘子
 */
router.post('/startRound', function (req, res, next) {
    var openid = req.body.openid
    var amount = parseInt(req.body.amount)//橘子的数量
    var slogen = req.body.slogen//口号

    console.log("startRound 创建:", openid, "-", amount, "-", slogen)

    var balance = 0;
    var new_balance = 0;

    var CURRENT_USER = null
    userDbUtils.getUserInfoByOpenid(openid, function (result) {
        if (result.length == 0) {
            res.send({
                'result': -1,
                'msg': 'openid错误'
            })
            console.log("ERROR -- openid错误")
            return
        }
        //拿到用户信息
        CURRENT_USER = result[0]
        balance = parseInt(CURRENT_USER.orange)//原来余额
        new_balance = balance - amount//新余额
        console.log('INFO -- 用户橘子余额为:', balance)
        console.log("INFO -- 新余额为:", new_balance)
        if (new_balance < 0) {
            res.send({
                'result': -2,
                'msg': '橘子余额不足'
            })
            console.log("ERROR -- 橘子余额不足")
            return
        }
        //创建一局游戏
        if (slogen == "" || slogen == null) {
            slogen = "爸爸给你买了橘子"
        } else if (slogen.length > 15) {
            res.send({
                'result': -3,
                'msg': '口号不能超过15个字!'
            })
            console.log("ERROR -- 口号不能超过15个字")
            return
        }
        console.log("条件允许, 开始创建round")

        dbUtils.doTranStartRound(openid, amount, slogen, new_balance, CURRENT_USER.balance, function (error) {
            res.send({
                'result': -4,
                'msg': '系统操作失败,发送橘子!' + error
            })
        }, function (rid) {
            res.send({
                'result': 0,
                'data': {
                    'id': rid,
                    'orange': amount,
                    'solgen': slogen
                }
            })
            sendTMsgUtils.doMSG_sendsuccess(openid, rid, CURRENT_USER.nickName, myConfig.gif.enable)
            return
        })
    })

});


/**
 * 查询某一局游戏
 */
router.post('/getRoundById', function (req, res, next) {
    var id = req.body.rid;

    var CURRENT_ROUND = null;
    var CURRENT_OWNER = null;
    var CURRENT_STATE = 0;
    gameDbUtils.getGameByGid(id, function (results, fields) {
        if (results.length == 0) {
            res.send({
                'result': -1,
                'msg': '查无此局游戏'
            })
            return
        }
        CURRENT_ROUND = results[0]
        //查询主人的信息
        userDbUtils.getUserInfoByOpenid(CURRENT_ROUND.uid, function (results, fields) {
            if (results.length == 0) {
                res.send({
                    'result': -2,
                    'msg': '查无此局游戏的创建人'
                })
                return
            }
            CURRENT_OWNER = results[0]
            //查询领取情况
            partakeDbUtils.getPartakesByRid(id, function (results, fields) {
                if(CURRENT_ROUND.state == 3){
                    CURRENT_STATE = 2;
                }else if(results.length == parseInt(CURRENT_ROUND.total)){
                    CURRENT_STATE = 1;
                }else{
                    CURRENT_STATE = 0;
                }
                if (results.length == 0) {
                    //本局游戏没有被人领取, 直接就返回了
                    res.send({
                        'result': 0,
                        'data': {
                            'owner_name': CURRENT_OWNER.nickName,
                            'owner_avatar': CURRENT_OWNER.avatarUrl,
                            'orange_total': CURRENT_ROUND.total,
                            'orange_received': CURRENT_ROUND.received,
                            'state': CURRENT_ROUND.state,
                            'time': timeUtils.getTimeMsg(CURRENT_ROUND.time),
                            'getList': []
                        }
                    })
                    return
                } else {
                    //本局游戏被人领取, 继续查找领取这的信息name, avatar, time
                    var partakeList = results
                    partakeList.reverse()//倒叙一下
                    var getList = []
                    async.forEachOf(partakeList, function (value, key, callback) {
                        userDbUtils.getUserInfoByOpenid(value.uid, function (results, fields) {
                            if (results.length > 0) {
                                var received_user = {}
                                received_user.nickName = results[0].nickName
                                received_user.avatarUrl = results[0].avatarUrl
                                received_user.call = value.subject
                                received_user.time = timeUtils.getTimeMsg(value.time)
                                getList.push(received_user)
                            } else {
                                res.send({
                                    'result': -4,
                                    'msg': '系统错误: 某个用户查询失败了'
                                })
                                return
                            }
                            callback()
                        })
                    }, function (error) {
                        if (error) {
                            res.send({
                                'result': -3,
                                'msg': '系统错误: 用户领取情况查询失败'
                            })
                            return
                        }
                        //这里才是真的返回啦, 完结撒花
                        res.send({
                            'result': 0,
                            'data': {
                                'owner_name': CURRENT_OWNER.nickName,
                                'owner_avatar': CURRENT_OWNER.avatarUrl,
                                'orange_total': CURRENT_ROUND.total,
                                'orange_received': partakeList.length,
                                'state': CURRENT_STATE,
                                'time': timeUtils.getTimeMsg(CURRENT_ROUND.time),
                                'getList': getList
                            }
                        })
                    })
                }
            })
        })
    })
})

/**
 * 获取我的所有游戏
 */
router.post('/getMyRounds', function (req, res, next) {
    var openid = req.body.openid;

    if (openid == "") {
        res.send({
            'result': -1,
            'msg': 'openid为空'
        })
        return
    }
    var myRoundList = []
    gameDbUtils.getRoundsByOpenid(openid, function (results, fields) {
        myRoundList = results
        myRoundList.reverse()
        async.forEachOf(myRoundList, function (value, key, callback) {
            partakeDbUtils.getPartakesCountByRid(value.id, function (results, fields) {
                myRoundList[key].received = results[0].count
                myRoundList[key].time = timeUtils.getTimeMsg(myRoundList[key].time)
                callback();//结束
            })
        }, function (error) {
            res.send({
                'result': 0,
                'data': myRoundList
            })
            return
        })
    })

})


module.exports = router;
