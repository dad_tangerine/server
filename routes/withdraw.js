var express = require('express');
var router = express.Router();

var timeUtils = require('../utils_common/timeutils')

var withdrawDbUtils = require('../utils_db/withdrawdbutils')


router.post('/getMyWithDraw', function (req, res, next) {
    var openid = req.body.openid

    if (openid == undefined || openid == "") {
        res.send({
            result: -1,
            msg: "openid不合法"
        })
        return
    }
    var data = []

    withdrawDbUtils.findWithdrawByOpenid(openid, function (results, fields) {
        for (var i = 0; i < results.length; i++) {
            var wd = {}
            wd.id = timeUtils.getDateToStringYYYYMMDD(results[i].time)+results[i].id
            wd.time = timeUtils.getTimeMsg(results[i].time)
            if (parseInt(results[i].state) == 0 || parseInt(results[i].state) == 2) {
                wd.state = "待发货"
            } else if (parseInt(results[i].state) == 3) {
                wd.state = "已发货"
            } else {
                wd.state = "异常"
            }
            wd.amonut = 50
            data.push(wd)
        }
        data.reverse()
        res.send({
            result: 0,
            data: data
        })
        return
    })
})

module.exports = router;