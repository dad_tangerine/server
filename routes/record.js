var express = require('express');
var router = express.Router();
var async = require('async')

var recordDbUtils = require('../utils_db/recorddbutils')
var userDbUtils = require('../utils_db/userdbutils')

var timeUtils = require('../utils_common/timeutils')

var myConfig = require('../config')


/**
 * 获取我的收入
 * openid 我的openid
 */
router.post("/getIncome", function (req, res, next) {
    var openid = req.body.openid;
    var page = parseInt(req.body.page);
    var size = parseInt(req.body.size);

    var recordList = null
    var total_income = 0
    var getList = []

    recordDbUtils.getRecordIncome(openid, page, size, function (results, fields) {
        recordList = results
        console.log("我的收入:共=", recordList.length)
        recordDbUtils.getIncomeTotal(openid, function (results, fields) {
            total_income = results[0].total

            // 补充用户信息(nickName, avatarUrl), 补充总数, 重新定义时间格式
            async.forEachOf(recordList, function (value, key, callback) {
                userDbUtils.getUserInfoByOpenid(value.from_openid, function (results, fields) {
                    var temp_user = results[0]
                    var item = {
                        nickName: temp_user.nickName,
                        avatarUrl: temp_user.avatarUrl,
                        des: value.des,
                        call: getRoundCall(),
                        time: timeUtils.getTimeMsg(value.time)
                    }
                    getList.push(item)
                    callback()
                })
            }, function (error) {
                if (error) {
                    res.send({
                        result: -1,
                        msg: "系统错误"
                    })
                    return
                }
                res.send({
                    result: 0,
                    data: {
                        total: total_income,
                        getList: getList
                    }
                })
                return
            })
        })
    })
})


/**
 * 获取我的支出
 * openid 我的openid
 */
router.post("/getOutcome", function (req, res, next) {
    var openid = req.body.openid;
    var page = parseInt(req.body.page);
    var size = parseInt(req.body.size);
    console.log("getOutcome接口的参数:", openid, "-", page, "-", size)

    var recordList = null
    var total_outcome = 0
    var getList = [];
    recordDbUtils.getRecordOutcome(openid, page, size, function (results, fields) {
        recordList = results
        console.log("我的支出:共=", recordList.length)
        recordDbUtils.getOutcomeTotal(openid, function (results, fields) {
            total_outcome = results[0].total

            //补充用户信息(nickName, avatarUrl), 补充总数, 重新定义时间格式
            async.forEachOf(recordList, function (value, key, callback) {
                console.log("  openid:", value.openid)
                console.log("f_openid:", value.from_openid)
                userDbUtils.getUserInfoByOpenid(value.openid, function (results, fields) {
                    var temp_user = results[0]
                    console.log("x_openid:", temp_user)
                    var item = {
                        nickName: temp_user.nickName,
                        avatarUrl: temp_user.avatarUrl,
                        des: value.des,
                        call: getRoundCall(),
                        time: timeUtils.getTimeMsg(value.time)
                    }
                    getList.push(item)
                    callback()
                })
            }, function (error) {
                if (error) {
                    res.send({
                        result: -1,
                        msg: "系统错误"
                    })
                    return
                }
                res.send({
                    result: 0,
                    data: {
                        total: total_outcome,
                        getList: getList
                    }
                })
                return
            })
        })
    })
})

function getRoundCall(){
    return myConfig.calls[parseInt(Math.random()*myConfig.calls.length)]
}

module.exports = router;