var express = require('express');
var router = express.Router();
var uuidv1 = require('uuid/v1')
var convert = require('xml-js');

var myConfig = require('../config')

//数据库帮助类
var gameDbUtils = require('../utils_db/gamedbutils')
var tradeDbUtils = require('../utils_db/tradedbutils')
var userDbUtils = require('../utils_db/userdbutils')
var withDbUtils = require('../utils_db/transcation_withdraw')
var withDrawDbTils = require('../utils_db/withdrawdbutils')

var transcationMoney = require('../utils_db/transcation_money')

//支付帮助类
var payUntils = require('../utils_http/pay_unifiedorder')
var payDataUtils = require('../utils_http/pay_util')

//普通帮助类
var timeUtils = require('../utils_common/timeutils')

/**
 * 充值, 购买橘子接口
 */
router.post("/pay", function (req, res, next) {
    var openid = req.body.openid//用户id
    var total_fee = req.body.amount//充值金额, 单位:分
    var detail = req.body.detail//订单说明
    var priceId = req.body.priceId//价格表中的序号
    var type = req.body.type
    var ornage = 0

    console.log("充值接口参数:", openid, "-", total_fee, "-", detail, "-", priceId)

    var payObj = {
        appid: myConfig.appid,
        body: 'wx-orange',
        mch_id: myConfig.mch_id,
        nonce_str: uuidv1().replace(/-/g, ""),
        notify_url: myConfig.notify_url,
        openid: openid,
        attach: JSON.stringify({type: 0}),//type> 0: 充值, 1: 凑60包邮, 2: 自付邮费
        out_trade_no: uuidv1().replace(/-/g, ""),
        spbill_create_ip: getClientIp(req),
        total_fee: total_fee,
        trade_type: 'JSAPI'
    }
    var dataJson = null

    var CURRENT_USER = null;
    //首先查询一下该用户是否存在
    userDbUtils.getUserInfoByOpenid(openid, function (results, fields) {
        if (results.length == 0) {
            res.send({
                'result': -1,
                'msg': '该用户不存在'
            })
            return
        }
        CURRENT_USER = results[0]

        //判断价格是否合法
        for (var i in myConfig.price_list) {
            var priceItem = myConfig.price_list[i]
            if (priceItem.id == priceId) {
                if (priceItem.price != total_fee) {
                    res.send({
                        'result': -2,
                        'msg': '本次购买橘子价格不合法!'
                    })
                    return
                } else {
                    orange = priceItem.amonut
                }
                break
            }
        }
        //开始发起请求 统一下单
        var sortedObj = payDataUtils.sortJson(payObj)
        console.log("排序后的json数据为>>>")
        console.log(sortedObj)
        var stringA_Temp = payDataUtils.get_stringA_Temp(sortedObj, myConfig.api_key)
        console.log("stringA_Temp数据为>>>")
        console.log(stringA_Temp)
        var sign = payDataUtils.get_sign(stringA_Temp)
        console.log("得到的签名为>>>")
        console.log(sign)
        var postXml = payDataUtils.get_POST_XML(sortedObj, sign)
        console.log("向微信下单的参数为>>>")
        console.log(postXml)
        payUntils.pay(postXml, function (data) {
            dataJson = JSON.parse(convert.xml2json(data, {compact: true, spaces: 1}))
            console.log("咱们输出看看dataJson的值:", dataJson)
            if (dataJson.xml.return_code._cdata != "SUCCESS" || dataJson.xml.return_msg._cdata != "OK") {
                res.send({
                    'result': -4,
                    'msg': "统一下单失败!"
                })
                console.log("统一下单接口失败: ", dataJson.xml.return_msg._cdata)
                return
            }
            //记录充值订单 trade
            tradeDbUtils.createTrade(payObj.openid, payObj.out_trade_no, payObj.total_fee, orange, detail, dataJson.xml.prepay_id._cdata, payObj.nonce_str, function (resluts, fields) {
                //返回值给客户端

                var returnObj = {
                    appId: myConfig.appid,
                    timeStamp: timeUtils.getTimeStamp(),
                    nonceStr: uuidv1().replace(/-/g, ""),
                    package: 'prepay_id=' + dataJson.xml.prepay_id._cdata,
                    signType: "MD5"
                }
                console.log("支付接口, [return阶段]待签名的数据为: ", returnObj)
                var sortedObj2 = payDataUtils.sortJson(returnObj)
                var stringA_Temp2 = payDataUtils.get_stringA_Temp(sortedObj2, myConfig.api_key)
                var sign2 = payDataUtils.get_sign(stringA_Temp2)
                console.log("支付接口, [return阶段]签名位: ", sign2)

                res.send({
                    'result': 0,
                    'data': {
                        timeStamp: returnObj.timeStamp,
                        nonceStr: returnObj.nonceStr,
                        package: returnObj.package,
                        signType: returnObj.signType,
                        paySign: sign2
                    }
                })
                return
            })
        }, function (error) {
            console.log("统一下单-ERROR:", error)
            res.send({
                'result': -3,
                'msg': '微信服务器报错了!'
            })
            return
        })

    })


})

/**
 * 提现接口
 */
router.post('/withDraw', function (req, res, next) {
    var openid = req.body.openid
    var number = parseInt(req.body.orange_number)
    var weight = req.body.orange_weight
    var addr = req.body.addr
    var tel = req.body.tel
    var consignee = req.body.consignee
    var money = parseInt(req.body.money)//费用, 不需要则为0
    var orange_account = parseInt(req.body.orange_account)//当前账户橘子个数
    var type = req.body.type;//提现方式

    console.log("提现接口:参数:", openid, "-", number, "-", weight, "-", addr, "-", tel, "-", consignee, "-", money, "-", orange_account, "-", type)

    if (openid == "" || number == "" || weight == "" || addr == "" || tel == "" || consignee == "") {
        res.send({
            'result': -1,
            'msg': '参数不完整'
        })
        return
    }
    var CURRENT_USER = null
    var new_orange = -1;
    userDbUtils.getUserInfoByOpenid(openid, function (results, fields) {
        if (results.length == 0) {
            res.send({
                'result': -1,
                'msg': '未找到该用户'
            })
            return
        }
        CURRENT_USER = results[0]
        new_orange = parseInt(CURRENT_USER.orange) - number
        if (type == 0) {//包邮
            console.log("提现接口: type=0, 继续进行中.....")
            if (new_orange < 0 || number < myConfig.orange_min) {
                res.send({
                    'result': -3,
                    'msg': '无法包邮, 橘子不够或者数量小于最小包邮数'
                })
                return
            }
            console.log("提现, new_orange是多少:", new_orange)
            withDbUtils.addWithDraw(openid, number, new_orange, addr, tel, consignee, CURRENT_USER.balance, function () {
                res.send({
                    'result': 0
                })
                return
            }, function (error) {
                res.send({
                    'result': -4,
                    'msg': '系统错误: 提现失败!' + error
                })
                return
            })

        } else if (type == 1) {//凑齐60个包邮
            console.log("提现接口: type=1, 继续进行中.....")
            if (number != myConfig.orange_min) {
                res.send({
                    'result': -5,
                    'msg': 'type=1>ERROR: 提现数量不是包邮数'
                })
                return
            }
            if (CURRENT_USER.orange >= 60) {
                res.send({
                    'result': -5,
                    'msg': 'type=1>ERROR: 用户余额大于包邮数, 不需要凑单!'
                })
                return
            }
            var money_2 = reckon_money_by_orange(number - CURRENT_USER.orange)
            if (money_2 != money) {
                res.send({
                    'result': -5,
                    'msg': 'type=1>ERROR: 计算应付凑单金额不符'
                })
                return
            }
            //记录订单withdraw
            withDrawDbTils.addWithDraw(type, openid, number, money, CURRENT_USER.orange, addr, tel, consignee, function (results, filed) {
                var withdraw_id = results.insertId
                //发起支付请求
                var payObj = {
                    appid: myConfig.appid,
                    body: 'wx-money',
                    mch_id: myConfig.mch_id,
                    nonce_str: uuidv1().replace(/-/g, ""),
                    notify_url: myConfig.notify_url,
                    openid: openid,
                    attach: JSON.stringify({type: 1, withdraw_id: withdraw_id}),//此接口都是1吧
                    out_trade_no: uuidv1().replace(/-/g, ""),
                    spbill_create_ip: getClientIp(req),
                    total_fee: money,
                    trade_type: 'JSAPI'
                }
                //开始发起请求 统一下单
                var sortedObj = payDataUtils.sortJson(payObj)
                var stringA_Temp = payDataUtils.get_stringA_Temp(sortedObj, myConfig.api_key)
                var sign = payDataUtils.get_sign(stringA_Temp)
                var postXml = payDataUtils.get_POST_XML(sortedObj, sign)
                payUntils.pay(postXml, function (data) {
                    dataJson = JSON.parse(convert.xml2json(data, {compact: true, spaces: 1}))
                    if (dataJson.xml.return_code._cdata != "SUCCESS" || dataJson.xml.return_msg._cdata != "OK") {
                        res.send({
                            'result': -4,
                            'msg': "统一下单失败!"
                        })
                        console.log("统一下单接口失败: ", dataJson.xml.return_msg._cdata)
                        return
                    }
                    //记录trade数据
                    tradeDbUtils.createTrade(openid, payObj.out_trade_no, money, 0, "凑60个包邮", dataJson.xml.prepay_id._cdata, payObj.nonce_str, function (resuts, fields) {
                        var returnObj = {
                            appId: myConfig.appid,
                            timeStamp: timeUtils.getTimeStamp(),
                            nonceStr: uuidv1().replace(/-/g, ""),
                            package: 'prepay_id=' + dataJson.xml.prepay_id._cdata,
                            signType: "MD5"
                        }
                        console.log("支付接口, [return阶段]待签名的数据为: ", returnObj)
                        var sortedObj2 = payDataUtils.sortJson(returnObj)
                        var stringA_Temp2 = payDataUtils.get_stringA_Temp(sortedObj2, myConfig.api_key)
                        var sign2 = payDataUtils.get_sign(stringA_Temp2)
                        console.log("支付接口, [return阶段]签名位: ", sign2)

                        res.send({
                            'result': 200,
                            'data': {
                                timeStamp: returnObj.timeStamp,
                                nonceStr: returnObj.nonceStr,
                                package: returnObj.package,
                                signType: returnObj.signType,
                                paySign: sign2
                            }
                        })
                        return
                    })
                }, function (error) {
                    console.log("统一下单-ERROR:", error)
                    res.send({
                        'result': -3,
                        'msg': '微信服务器报错了!'
                    })
                    return
                })
            })

        } else if (type == 2) {//自付邮费
            console.log("提现接口: type=2, 继续进行中.....")
            if (new_orange < 0) {
                res.send({
                    'result': -5,
                    'msg': 'type=2>ERROR:用户余额不足, 无法付费提现'
                })
                return
            }
            var money_2 = rekon_postage_by_orange(number)
            if (money_2 != money) {
                res.send({
                    'result': -5,
                    'msg': 'type=2>ERROR: 计算应付邮费金额不符'
                })
                return
            }
            //创建订单
            withDrawDbTils.addWithDraw(2, openid, number, money, number, addr, tel, consignee, function (results, fileds) {
                var withdraw_id = results.insertId
                //发起支付请求
                var payObj = {
                    appid: myConfig.appid,
                    body: 'wx-orange',
                    mch_id: myConfig.mch_id,
                    nonce_str: uuidv1().replace(/-/g, ""),
                    notify_url: myConfig.notify_url,
                    openid: openid,
                    attach: JSON.stringify({type: 1, withdraw_id: withdraw_id}),//此接口都是1吧
                    out_trade_no: uuidv1().replace(/-/g, ""),
                    spbill_create_ip: getClientIp(req),
                    total_fee: money,
                    trade_type: 'JSAPI'
                }
                //开始发起请求 统一下单
                var sortedObj = payDataUtils.sortJson(payObj)
                var stringA_Temp = payDataUtils.get_stringA_Temp(sortedObj, myConfig.api_key)
                var sign = payDataUtils.get_sign(stringA_Temp)
                var postXml = payDataUtils.get_POST_XML(sortedObj, sign)
                payUntils.pay(postXml, function (data) {
                    dataJson = JSON.parse(convert.xml2json(data, {compact: true, spaces: 1}))
                    //记录trade数据
                    tradeDbUtils.createTrade(openid, payObj.out_trade_no, money, 0, "自付邮费", dataJson.xml.prepay_id._cdata, payObj.nonce_str, function (resuts, fields) {
                        var returnObj = {
                            appId: myConfig.appid,
                            timeStamp: timeUtils.getTimeStamp(),
                            nonceStr: uuidv1().replace(/-/g, ""),
                            package: 'prepay_id=' + dataJson.xml.prepay_id._cdata,
                            signType: "MD5"
                        }
                        console.log("支付接口, [return阶段]待签名的数据为: ", returnObj)
                        var sortedObj2 = payDataUtils.sortJson(returnObj)
                        var stringA_Temp2 = payDataUtils.get_stringA_Temp(sortedObj2, myConfig.api_key)
                        var sign2 = payDataUtils.get_sign(stringA_Temp2)
                        console.log("支付接口, [return阶段]签名位: ", sign2)

                        res.send({
                            'result': 200,
                            'data': {
                                timeStamp: returnObj.timeStamp,
                                nonceStr: returnObj.nonceStr,
                                package: returnObj.package,
                                signType: returnObj.signType,
                                paySign: sign2
                            }
                        })
                        return
                    })
                }, function (error) {
                    console.log("统一下单-ERROR:", error)
                    res.send({
                        'result': -3,
                        'msg': '微信服务器报错了!'
                    })
                    return
                })

            })
            //发起支付
        } else {
            res.send({
                'result': -10,
                'msg': '这种包邮,我都没听说过'
            })
            return
        }
    })

})


/**
 * 异步通知结果的接口
 */
router.post("/notify_pay_result", function (req, res, next) {
    var res_str = "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>"
    var temp = req.body.xml
    var notifyObj = {}
    var sign = ""
    var attachObj = {}
    console.log("支付回调: 数据=>>>>\n", temp)

    for (var key in temp) {
        if (key != "sign") {
            notifyObj[key] = temp[key][0]
        } else {
            sign = temp[key][0]
        }
    }
    attachObj = JSON.parse(notifyObj.attach)
    console.log("支付回调: 订单号为out_trade_no:", notifyObj.out_trade_no)
    var CURRENT_TRADE = null
    var CURRENT_USER = null
    var CURRENT_WITHDRAW = null
    if (notifyObj.result_code == "SUCCESS" && notifyObj.return_code == "SUCCESS") {
        var sortedObj = payDataUtils.sortJson(notifyObj)
        var stringA_Temp = payDataUtils.get_stringA_Temp(sortedObj, myConfig.api_key)
        var mySign = payDataUtils.get_sign(stringA_Temp)
        if (mySign != sign) {
            res.send("小臂崽子, 别让我逮到你!>>>", getClientIp(req))
            return
        }
        //查找订单
        tradeDbUtils.findTrade(notifyObj.openid, notifyObj.out_trade_no, notifyObj.total_fee, notifyObj.nonce_str, function (results, fields) {
            if (results.length == 1) {
                CURRENT_TRADE = results[0]
                if (CURRENT_TRADE.state == 1) {
                    console.log("支付回调: 此订单已经处理, 不再理会!")
                    res.send(res_str)
                    return
                }

                userDbUtils.getUserInfoByOpenid(notifyObj.openid, function (results, fields) {
                    if (results.length != 1) {
                        console.log("支付回调: 没找到订单对应的用户, 完蛋!")
                    }
                    CURRENT_USER = results[0]

                    CURRENT_WITHDRAW = results[0]
                    if (attachObj.type == 0) {//属于购买, 直接加橘子, 扣款
                        console.log("支付回调: type=0, 继续进行中.....")
                        transcationMoney.moneySuccess(CURRENT_USER, CURRENT_TRADE, function () {
                            res.send(res_str)
                            return
                        }, function (error) {
                        })
                    } else if (attachObj.type == 1 || attachObj.type == 2) {
                        console.log("支付回调: type=1/2, 继续进行中.....")
                        withDrawDbTils.findWithdrawById(attachObj.withdraw_id, function (results, fields) {
                            if (results.length != 1) {
                                console.log("支付回调: 用来提现的支付,找不到提现订单, 扑街了")
                            }
                            transcationMoney.moneySuccess_withdraw(CURRENT_USER, CURRENT_TRADE, CURRENT_WITHDRAW, function () {
                                res.send(res_str)
                                return
                            }, function (error) {

                            })
                        })
                    } else {
                        console.log('这个支付回调根本不知道用来干嘛的, 没法处理')
                    }
                })
            } else {
                console.log("支付回调: 没找到此订单, 无法上分!")
            }
        })

    } else {
        console.log("支付回调: 微信说失败了")
        res.send(res_str)
        return
    }

})

function getClientIp(req) {
    return req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
};

/**
 * 根据橘子数量计算邮费
 * @param orange 橘子个数
 */
function rekon_postage_by_orange(orange) {
    if (orange <= 20) {
        return 1500
    } else {
        if ((orange - 20) % 500 == 0) {
            return (orange - 20) * 200 / 5 + 1500
        } else {
            return Math.ceil((orange - 20) / 5) * 200 + 1500
        }
    }
}

/**
 * 根据橘子数量计算金额
 */
function reckon_money_by_orange(orange) {
    var orange = orange;
    var money = 0;
    var priceList = myConfig.price_list
    while (orange > 0) {
        for (var i = priceList.length - 1; i >= 0; i--) {
            var priceItem = priceList[i]
            if (orange >= priceItem.amonut) {
                money += priceItem.price
                orange -= priceItem.amonut
                break;
            }
        }
        if (orange < priceList[0].amonut) {
            money += (priceList[0].price / priceList[0].amonut) * orange
            orange = 0;
        }
    }
    console.log(money)
    return money
}

module.exports = router;
