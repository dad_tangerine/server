var express = require('express');
var router = express.Router();
var async = require('async')

//网络请求帮助类
var code2Session = require('../utils_http/code2Session.js')

//数据库请求帮助类
var userDbUtils = require('../utils_db/userdbutils.js')
var recordDbUtils = require('../utils_db/recorddbutils')
var formmidDbUtils = require('../utils_db/formiddbutils')

//通用请求帮助类
var timeUtils = require('../utils_common/timeutils')

/* GET users listing. */

/**
 * 注册/激活
 */
router.post('/register', function (req, res, next) {
    var openid = req.body.openid
    var nickName = req.body.nickName
    var avatar = req.body.avatar
    var channel = req.body.channel
    var gender = req.body.gender //1: 男, 0: 女

    console.log(openid, "-", nickName, "-", avatar, "-", channel, "-", gender)

    if (openid == "") {
        res.send({
            'result': -1,
            'msg': 'openid为空'
        })
        return
    }
    userDbUtils.getUserInfoByOpenid(openid, function (results, fields) {
        if (results.length > 0) {
            //存在这个用户, 看看是否要修改信息
            res.send({
                'result': 0,
                'msg': '旧用户,欢迎回来'
            })
            return;
        } else {
            //新用户, 保存起来
            userDbUtils.register(openid, nickName, avatar, channel, gender, function (results, fields) {
                res.send({
                    'result': 0,
                    'msg': '新用户注册成功'
                })
                return
            })
        }
    })


})

/**
 * 获取用户信息
 */
router.post('/getUserInfo', function (req, res, next) {
    var openid = req.body.openid;
    console.log("获取用户信息接口:参数 openid=", openid)
    if (openid == "") {
        res.send({
            'result': -1,
            'msg': 'openid为空'
        })
        return
    }
    userDbUtils.getUserInfoByOpenid(openid, function (results, fields) {
        if (results.length > 0) {
            res.send({
                'result': 0,
                'data': results[0]
            })
            return
        } else {
            res.send({
                'result': -2,
                'msg': '查无此人信息!'
            })
            return
        }
    })
});

/**
 * 获取openid
 */
router.post('/getOpenId', function (req, res, next) {
    var code = req.body.code

    code2Session.getSession(code, function (data) {
        var dataJson = JSON.parse(data)
        console.log("我来看看这是啥", JSON.stringify(dataJson))
        if (dataJson.errcode) {
            res.send({
                'result': -1,
                'msg': '获取openid失败:errorcode=' + dataJson.errcode
            })
            return

        } else {
            res.send({
                'result': 0,
                'data': {
                    'openid': dataJson.openid
                }
            })
            return
        }
    }, function (error) {
        res.send({
            'result': -2,
            'msg': '获取opeinid失败: 系统错误!' + error
        })
        return
    })
});

/**
 * 获取个人消费记录
 * openid
 * type
 * page 页码,从0开始
 * size 每页条数
 */
router.post('/getWaterBills', function (req, res, next) {
    res.send("fuck you, bitch")
    return
    var openid = req.body.openid
    var type = parseInt(req.body.type)
    var page = parseInt(req.body.page)
    var size = parseInt(req.body.size)

    console.log("获取流水单:", openid, "-", type, "-", page, "-", size)

    if (openid == "") {
        res.send({
            'result': -1,
            'msg': 'openid为空'
        })
        return
    }
    //定义一个数据
    var recordList = []
    if (type == 0) {
        recordDbUtils.getRecords(openid, page, size, function (results, fields) {
            recordList = results;
            recordList.reverse()//倒叙
            //拿到我要的记录后, 把"被领取人"的nickName拿出来
            async.forEachOf(recordList, function (value, key, callback) {
                recordList[key].time = timeUtils.getTimeMsg(value.time)//首先,格式化时间
                if (value.from_openid != -1) {//如果是得到橘子, 得知道是从谁nickName那里来的
                    userDbUtils.getUserInfoByOpenid(value.from_openid, function (results, field) {
                        if (results.length > 0) {
                            recordList[key].nickName = results[0].nickName
                        } else {
                            recordList[key].nickName = "匿名"
                        }
                        callback()
                    })
                } else {
                    callback()
                }
            }, function (error) {
                res.send({
                    'result': 0,
                    'data': recordList
                })
                return
            })
        })
    } else {
        recordDbUtils.getRecordsByType(openid, type, page, size, function (results, fields) {
            recordList = results;
            recordList.reverse()//倒叙
            async.forEachOf(recordList, function (value, key, callback) {
                recordList[key].time = timeUtils.getTimeMsg(value.time)//首先,格式化时间
                if (value == 5 && value.from_openid != -1) {//如果是得到橘子, 得知道是从谁nickName那里来的
                    userDbUtils.getUserInfoByOpenid(value.from_openid, function (results, field) {
                        if (results.length > 0) {
                            recordList[key].nickName = results[0].nickName
                        } else {
                            recordList[key].nickName = "匿名"
                        }
                        callback()
                    })
                } else {
                    callback()
                }
            }, function (error) {
                res.send({
                    'result': 0,
                    'data': recordList
                })
                return
            })
        })
    }
})

/**
 * 保存用户地址接口
 */
router.post('/updateUserAddr', function (req, res, next) {
    var openid = req.body.openid
    var addr = req.body.addr

    if (openid == "" || addr == "") {
        res.send({
            'result': -1,
            'msg': '用户id或者addr为空'
        })
        return
    } else {
        userDbUtils.updateAddr(openid, addr, function (results, fields) {
            res.send({
                'result': 0
            })
        })
    }
})

/**
 * 获取formid接口
 */
router.post('/getFormId', function (req, res, next) {
    var openid = req.body.openid;
    var formid = req.body.formid;

    console.log("getFormId参数:", openid, "-", formid)

    formmidDbUtils.addFormid(openid, formid, function (results, fields) {
        res.send({
            'results': 0
        })
        return
    })
})

module.exports = router;
