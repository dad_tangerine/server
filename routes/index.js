var express = require('express');
var router = express.Router();

var fs = require('fs')

var config = require('../config')

var sendTMsgUtils = require('../utils_http/sendTMsgUtils')

/* GET home page. */

/**
 * 获取总配置
 */
router.post('/wx_api/getConfig', function (req, res, next) {

    res.send({
        'result': 0,
        'data': {
            "version": config.verion,
            'video_url': config.video_url,
            'gif':config.gif,
            'banner_url': 'http://orange.geinigejuzichi.top/banner.png',
            'calls': config.calls,
            'share_img_url': config.share_img_url,
            'share_msg': config.share_msg,
            "btn1_value": "发橘子给好友",
            'orange_min': config.orange_min,//最少提现的橘子数量
            'postage': config.postage,//邮费
            'price_list': config.price_list,
            'enable': false,
            "result_page_enable": config.result_page_enable
        }
    })
    return;
});


/**
 * 统计接口
 */
router.post('/wx_api/Statistics', function (req, res, next) {
    // res.render('index', {title: 'Express'});
    //TODO: 统计接口
    res.send({
        'result': 0,
        'data':'敬请期待.......'
    })
    return;
});

/**
 * 视频打开接口
 */
router.post('/wx_api/videoOpen',function (req, res, next) {
    res.send({
        'result': 0,
        'data': {
            'enable': false
        }
    })
})

/**
 * 测试接口
 */
router.post('/wx_api/test', function (req, res, next) {
    var openid = req.body.openid;
    sendTMsgUtils.doMSG_return(openid)
    return;
    
})
function getClientIp(req) {
    return req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
};

module.exports = router;
